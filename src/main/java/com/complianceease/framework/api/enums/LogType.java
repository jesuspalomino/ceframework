package com.complianceease.framework.api.enums;

import com.aventstack.extentreports.Status;

public enum LogType {
	
	  Error(Status.ERROR),
	  Info(Status.INFO),
	  Fail(Status.FAIL),
	  Pass(Status.PASS),
	  Skip(Status.SKIP),
	  Warning(Status.WARNING);
	
	private Status logStatus;
	
	private LogType(Status a) {
		 logStatus = a;
       }
	 
	public Status getLogStatus() {
         return logStatus;
       }

}
