package com.complianceease.framework.api.enums;

public class HttpHeader {
	public static final String Server = "Server";
	public static final String HttpStatusCode = null;
	public static final String CEResultCode = "CEResultCode";
	public static final String CacheControl = "Cache-Control";
	public static final String ContentType = "Content-Type"; 
	public static final String ContentEncoding = "Content-Encoding";
	public static final String Expires = "Expires";
	public static final String LastModified = "Last-Modified"; 
	public static final String Vary = "Vary"; 
	public static final String XAspNetMvcVersion = "X-AspNetMvc-Version";
	public static final String XAspNetVersion = "X-AspNet-Version";
	public static final String PoweredBy = "X-Powered-By";
	public static final String ResponseDateTime = "Date";
	public static final String ContentLength = "Content-Length";
	
	public static HttpHeader instance = null;
	
	private HttpHeader() {};
	
	static {
		
		try {
			instance = new HttpHeader();
		}
		catch (Exception ex) {
			System.err.println("Unable to contruct the object HttpHeader.");
		}
	}
	
	public static HttpHeader getInstance() {
		return instance;		
	}
}
