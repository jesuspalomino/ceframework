package com.complianceease.framework.api.enums;

public enum IntegratedDisclosureDocumentType {
    LOANESTIMATE,
    CLOSINGDISCLOSURE,
    POSTCONSUMMATIONREVISEDCLOSINGDISCLOSURE
}
