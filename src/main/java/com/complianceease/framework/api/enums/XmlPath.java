package com.complianceease.framework.api.enums;

public class XmlPath {
	
	public static final String F4506StatusFirstName = "/Result/Value/F4506Status[%d]/FirstName/text()";
	public static final String MessageValue = "/Messages/Message[%d]/Value/text()";
	public static final String ResultValue = "/Result/Value/text()";

	public static XmlPath instance = null;
	
	private XmlPath() {};
	
	static {
		
		try {
			instance = new XmlPath();
		}
		catch (Exception ex) {
			System.err.println("Unable to contruct the object XmlPath");
		}
	}
	
	public static XmlPath getInstance() {
		return instance;		
	}
}
