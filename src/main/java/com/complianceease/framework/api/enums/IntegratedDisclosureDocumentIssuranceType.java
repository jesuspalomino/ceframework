package com.complianceease.framework.api.enums;

public enum IntegratedDisclosureDocumentIssuranceType {
    INITIAL,
    REVISED
}
