package com.complianceease.framework.api.enums;

public enum DataFormat {
	XML,
	JSON,
	CSV
}
