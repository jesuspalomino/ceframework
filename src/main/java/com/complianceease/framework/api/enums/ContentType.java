package com.complianceease.framework.api.enums;

public enum ContentType {

	TEXT("text/plain"),

    JSON("application/json", "application/javascript", "text/javascript"),

    XML("application/xml", "text/xml", "application/xhtml+xml"),

    HTML("text/html"),

    URLENC("application/x-www-form-urlencoded"),

    BINARY("application/octet-stream"),
	
	MULTIPART("multipart/form-data"),
	
	TEXTJSON("text/json");

    private final String[] ctStrings;

    public String[] getContentTypeStrings() {
        return ctStrings;
    }

    private ContentType(String... contentTypes) {
        this.ctStrings = contentTypes;
    }
    
    @Override
    public String toString() {
        return ctStrings[0];
    }
}
