package com.complianceease.framework.api.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import javax.activation.MimetypesFileTypeMap;
import org.apache.commons.codec.binary.Base64;

public class Core {
	
	public static String getContentType(String filename) {
		MimetypesFileTypeMap fileTypeMap = new MimetypesFileTypeMap();
		return fileTypeMap.getContentType(filename);
	}
	
	public static String getExtension(String filename) {	 		
		if (filename == null) {
            return null;
        }
        int extensionPos = filename.lastIndexOf('.');
        int lastUnixPos = filename.lastIndexOf('/');
        int lastWindowsPos = filename.lastIndexOf('\\');
        int lastSeparator = Math.max(lastUnixPos, lastWindowsPos);
 
        int index = lastSeparator > extensionPos ? -1 : extensionPos;
        if (index == -1) {
            return "";
        } else {
            return filename.substring(index + 1);
        }
	}
	
	public static String convertToBase64String(String uploadfile) throws IOException {
        Path path = Paths.get(uploadfile);
        byte[] encoded = Base64.encodeBase64(Files.readAllBytes(path));
        String encodedString = new String(encoded);
        return encodedString;
	}
	
	//An alternative method to upload a file
	public String encodeFileToBase64String(String fileName) throws IOException {
		File file = new File(fileName);
		byte[] bytes = loadFile(file);
		byte[] encoded = Base64.encodeBase64(bytes);
		String encodedString = new String(encoded);
		return encodedString;
	}
  
	public byte[] loadFile(File file) throws IOException {
		
		InputStream is = new FileInputStream(file);

	    long length = file.length();
	    if (length > Math.pow(10, 7)) {
	    	 is.close();
	    	  throw new IOException("File must be less than 10 megabytes. ("+ file.getName() + " / File size : " + length + "bytes.");
	    }
	    byte[] bytes = new byte[(int)length];
	    
	    int offset = 0;
	    int numRead = 0;
	    while (offset < bytes.length
	           && (numRead=is.read(bytes, offset, bytes.length-offset)) >= 0) {
	        offset += numRead;
	    }

	    if (offset < bytes.length) {
	        is.close();
	        throw new IOException("Could not completely read file "+file.getName());
	    }

	    is.close();
	    return bytes;
	}
}
