package com.complianceease.framework.api.util;

import java.nio.ByteBuffer;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.EmptyStackException;
import java.util.UUID;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class StringUtil {

	public static String subStringRegex(String regex, String data)
	{
		Pattern pattern = Pattern.compile(regex);
		Matcher matcher = pattern.matcher(data);
		if(matcher.find())
		{
			return matcher.group(); 	
		}
		else
		{
			throw new EmptyStackException();
		}
	}
	
	 public static String getUUID() {
		 
   	  UUID uuid = UUID.randomUUID();
   	  long l = ByteBuffer.wrap(uuid.toString().getBytes()).getLong();
   	  return Long.toString(l, Character.MAX_RADIX);
   	  
   	}
	 
	 public static Boolean isNullOrEmpty(String value)
		{
			
		   if(value.isEmpty() || value == null)
			   return true;
		   else
			   return false;
					
		}
}







