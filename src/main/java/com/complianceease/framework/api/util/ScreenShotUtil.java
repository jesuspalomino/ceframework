package com.complianceease.framework.api.util;

import java.io.File;
import java.io.IOException;
import java.util.UUID;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import com.complianceease.framework.api.enums.LogType;
import com.complianceease.framework.common.ReportFactory;

public class ScreenShotUtil {
	
	public static void captureScreenshot(WebDriver driver)
	{
		try
		{	
			UUID uuid = UUID.randomUUID();
			String img =  uuid +".png";
			TakesScreenshot ts = (TakesScreenshot)driver;
		    File source = ts.getScreenshotAs(OutputType.FILE);
		    FileUtils.copyFile(source, new File("./output/TestCaseResults/screenshots/"+ img));
		    ReportFactory.insertScreenShot(img);	   
		}
		catch(IOException ex)
		{
			System.out.println("Exception while taking screenshot"+ ex.getMessage());
			ReportFactory.printAndLog(LogType.Error, "Exception while taking screenshot: </br>"+ ex.getMessage());
		}
	}
}
