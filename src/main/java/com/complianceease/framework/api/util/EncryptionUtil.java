package com.complianceease.framework.api.util;

import java.io.IOException;

public class EncryptionUtil {

	public static String EncryptCsvToElf (String path)
	{
	  	Runtime rt = Runtime.getRuntime();
	  	String elfFile = path + ".lef";
	  	try {
	  		System.out.println ("elfFile Location :"+elfFile);
	  		System.out.println("Path :"+path);
	  		
			Process pEncryptLEF = rt.exec("c:\\gnupg\\gpg.exe --encrypt --quiet --always-trust --recipient it@complianceease.com --output "
			+"\""+ elfFile +"\""+ " " + "\""+path+"\"");
			
			System.out.println(pEncryptLEF);
		} catch (IOException e) {
           e.printStackTrace();
		}	
	  	
	  	return elfFile;
	}
}
