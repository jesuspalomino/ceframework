package com.complianceease.framework.api.util;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;

import com.complianceease.framework.api.enums.LogType;
import com.complianceease.framework.common.ReportFactory;


public class ExcelUtils {

	private Sheet worksheet;

	private Workbook workbook;

	private Cell selectedCell;

	private Row selectedRow;
	
	private Row tableHeader;

	private File testDataFile;
	
	public ExcelUtils(){};

	public ExcelUtils(File testDataFile) 
			throws EncryptedDocumentException, InvalidFormatException, IOException {
		try {
		this.testDataFile = testDataFile;
		System.out.println("File Absolute Path"+this.testDataFile.getAbsolutePath().replaceAll("%20", " "));
		this.testDataFile = new File(this.testDataFile.getAbsolutePath().replaceAll("%20", " "));
		this.workbook = WorkbookFactory.create(this.testDataFile);
		this.worksheet = this.workbook.getSheetAt(0);
		tableHeader = this.worksheet.getRow(0);
		}catch(Exception ex) {
			System.out.println("Got Exception --->");
			ex.printStackTrace();
			throw ex;
		}
		
	}
	
	public void setActiveWorksheetByName(String sheetName) throws EncryptedDocumentException, InvalidFormatException, IOException {
		
		try {
			System.out.println("Loading File Name : " + testDataFile.getName());
			System.out.println("Loading Worksheet Name : " + sheetName);

			this.worksheet = this.workbook.getSheet(sheetName);
			tableHeader = this.worksheet.getRow(0);
		}
		catch (Throwable ex) {
			System.err.println(ex.getMessage() + ex.getStackTrace());
		}
	}
	
	public String getCellData(int rowNum, int colNum) {
		String cellData = "";
		
		selectedCell = this.worksheet.getRow(rowNum).getCell(colNum);
		
		if(selectedCell != null &&
				selectedCell.getCellType() != Cell.CELL_TYPE_BLANK)
			cellData = selectedCell.getStringCellValue().trim();
		
		return cellData;
    }

	public void setCellData(String result,  int rowNum, int colNum) throws IOException {
		selectedRow  = this.worksheet.getRow(rowNum);
		selectedCell = selectedRow.getCell(colNum, Row.RETURN_BLANK_AS_NULL);

		if (selectedCell == null) {
			selectedCell = selectedRow.createCell(colNum);
			selectedCell.setCellValue(result);

		} 
		else {
			selectedCell.setCellValue(result);
		}

		// Constant variables Test Data path and Test Data file name

		FileOutputStream fileOut = new FileOutputStream(testDataFile.toString());

		this.workbook.write(fileOut);

		fileOut.flush();

		fileOut.close();
	}
	
	public int getLastRowNum()
	{
		//Gets the number last row on the sheet.
		return this.worksheet.getLastRowNum();
	}
	
	public int getRowLastColumNum(int rowNum)
	{
		//Gets the index of the last cell contained in this row PLUS ONE.
		return this.worksheet.getRow(rowNum).getLastCellNum();
	}

	public Object[] getTestDataPerRowAsObject(int rowNum)
	{
		List<String> list = new ArrayList<String>();
       
        Row row = this.worksheet.getRow(rowNum); 
        Iterator<Cell> cellIterator = row.cellIterator();
        
        while(cellIterator.hasNext()) {
        	Cell cell = cellIterator.next();
        	cell.setCellType(Cell.CELL_TYPE_STRING); 
    		String cellData = tableHeader.getCell(cell.getColumnIndex()) + ":" + cell.getStringCellValue().trim();
    		list.add(cellData);
        }
                
        return list.toArray();
	}
	
	public Object[] getTestDataHeader()
	{
        return getTestDataPerRowAsObject(0);
	}
                
	public List<Object[]> getTestDatabyTestCaseName(String testCaseName)
	{        
		List<Object[]> testData = new ArrayList<Object[]>();
		
		int rowCount = this.worksheet.getLastRowNum();
		
		for(int i = 1; i <= rowCount; i++)
		{
			if(this.getCellData(i, 2) == testCaseName)
				testData.add(this.getTestDataPerRowAsObject(i));
		}
		
        return testData;
	}
	
	public List<String> getTestDataPerRow(int rowNum)
	{
       List<String> list = new ArrayList<String>();
       
        Row row = this.worksheet.getRow(rowNum + 1); //start from row #1 not zero 
        Iterator<Cell> cellIterator = row.cellIterator();

        while(cellIterator.hasNext()) {
        	Cell cell = cellIterator.next();
        	cell.setCellType(Cell.CELL_TYPE_STRING); 
    		String cellData = cell.getStringCellValue().trim();
    		list.add(cellData);
        }
                
        return list;
	}
	
    public Object[] getTestDataInRow(int rowNum)
	    {
	     	List<String> list = new ArrayList<String>();
	         
	          Row row = this.worksheet.getRow(rowNum); 
	          Iterator<Cell> cellIterator = row.cellIterator();
	         
	         while(cellIterator.hasNext()) {
	              Cell cell = cellIterator.next();
	              cell.setCellType(Cell.CELL_TYPE_STRING); 
	              String cellData = cell.getStringCellValue().trim();
	              list.add(cellData);
	          }
	                  
	          return list.toArray();
	      }

		
	public void close() throws IOException
	{
		this.workbook.close();
	}
	public static Map<String, String> getParameters(Object[] testData) throws IOException
	{
		Map<String, String> parameters = new HashMap<String, String>();

		for(int i = 0; i<testData.length; i++)
		{
			String cellData = testData[i].toString();
			
			if(cellData.isEmpty() == false && cellData.equals("null") == false)
        	{
	        	String parameter = null;
	        	String value = null;
	                	
	        	if(cellData.contains(":"))
	        	{
		        	String[] parts = cellData.split("[:]", 2); //this takes a regular expression, so remember to escape special characters if necessary. 
		        	parameter = parts[0];
		        	value = parts[1];
	        	}
	        	else
	        	{
	        		throw new IOException("testData missing [:]; index : " + i );
	        	}
	        	parameters.put(parameter, value);
        	}

		}	
		
		ReportFactory.printAndLog(LogType.Info,"<b>TestCaseId:</b> " + parameters.get("TestCaseId"));

		return parameters;
	}
}