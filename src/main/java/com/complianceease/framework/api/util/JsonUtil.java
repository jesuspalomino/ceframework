package com.complianceease.framework.api.util;

import java.util.ArrayList;
import java.util.Iterator;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.complianceease.framework.api.enums.LogType;
import com.complianceease.framework.common.ReportFactory;

public class JsonUtil {

    private static JSONObject jsonObject ;
    private JsonUtil(){

    }

    public static String getResponseValueByJSONNode(String jsonString,String nodeName){
        String[] nodes = nodeName.split("\\.");

        try {

            jsonObject=new JSONObject(jsonString);
            for (int i =0;i<nodes.length-1;i++)  {
                try{
                    jsonObject= jsonObject.getJSONObject(nodes[i]);
                }catch (JSONException e){
                    return null;
                }
            }
            return jsonObject.getString(nodes[nodes.length-1]);

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }
    
    public static Boolean getResponseBooleanValueByJSONNode(String jsonString,String nodeName){
        String[] nodes = nodeName.split("\\.");

        try {

            jsonObject=new JSONObject(jsonString);
            for (int i =0;i<nodes.length-1;i++)  {
                try{
                    jsonObject= jsonObject.getJSONObject(nodes[i]);
                }catch (JSONException e){
                    return null;
                }
            }
            return jsonObject.getBoolean((nodes[nodes.length-1]));

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }
    
    public static Double getResponseDoubleValueByJSONNode(String jsonString,String nodeName){
        String[] nodes = nodeName.split("\\.");

        try {

            jsonObject=new JSONObject(jsonString);
            for (int i =0;i<nodes.length-1;i++)  {
                try{
                    jsonObject= jsonObject.getJSONObject(nodes[i]);
                }catch (JSONException e){
                    return null;
                }
            }
            return jsonObject.getDouble((nodes[nodes.length-1]));

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static JSONArray getJsonArray(String jsonString,String nodeName){
        String[] nodes = nodeName.split("\\.");
        try {

            jsonObject = new JSONObject(jsonString);
            for(int i=0;i<nodes.length-2;i++){
                jsonObject= jsonObject.getJSONObject(nodes[i]);
            }

            return jsonObject.getJSONArray(nodes[nodes.length-1]);

        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }

    }    
    
    public static String[] getTextsfromJsonArray(String jsonBody,String arrayLocation,String nodeName)
    {	
    	String[] values= null;
    	
    	try
    	{
    		JSONArray errorsMessage = JsonUtil.getJsonArray(jsonBody, arrayLocation);
    		int length = errorsMessage.length();
    		values = new String[length];
    		Iterator<Object> itr = errorsMessage.iterator();
        	int i = 0;
    		while(itr.hasNext())
    		{
    			String node = itr.next().toString();
    			values[i] = getResponseValueByJSONNode(node, nodeName);
    			i += 1;		
    		}
    	}
    	catch(Exception ex)
    	{
    		ReportFactory.printAndLog(LogType.Error, "Error on getTextsfromJsonArray method: " + ex.getMessage());
    	}
    
    	return values;
    }
    
    
     public static String getTextfromJsonArray(String jsonBody, int arrayLocation, String nodeName) {
		
		JSONArray arrJSON = new JSONArray(jsonBody);
		JSONObject jsonData = new JSONObject(arrJSON.get(arrayLocation).toString());

		String result = jsonData.get(nodeName).toString();
		
		return result;
    	
    }
     
     public static String[] getNames(String jsonString)
     {
    	 jsonObject = new JSONObject(jsonString);
    	 String[] jsonName = JSONObject.getNames(jsonObject);
    	 
    	 return jsonName;
    	 
     }

    public static JSONObject getJsonObject() {
        return jsonObject;
    }

    public void setJsonObject(JSONObject jsonObject) {
        JsonUtil.jsonObject = jsonObject;
    }
}
