package com.complianceease.framework.api.util;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringReader;
import java.io.StringWriter;
import java.nio.charset.Charset;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.soap.MessageFactory;
import javax.xml.soap.MimeHeaders;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPMessage;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.TransformerFactoryConfigurationError;
import javax.xml.transform.dom.DOMResult;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import com.complianceease.framework.api.enums.LogType;
import com.complianceease.framework.common.ReportFactory;

public class XmlUtils {

	private static final Pattern HEADER_PATTERN = Pattern.compile("^\\<\\?.+\\?\\>(.*)", Pattern.DOTALL);

	private Document doc;
	private static SOAPMessage soapResponse;

	public XmlUtils(String xmlResponse) throws ParserConfigurationException, SAXException, IOException {
		DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
		docFactory.setNamespaceAware(false); // never forget this!
		docFactory.setValidating(false);
		DocumentBuilder builder = docFactory.newDocumentBuilder();
		doc = builder.parse(new InputSource(new StringReader(xmlResponse)));
	}

	public XmlUtils(InputStream is)
			throws FileNotFoundException, ParserConfigurationException, IOException, SAXException {
		DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
		docFactory.setNamespaceAware(false); // never forget this!
		docFactory.setValidating(false);
		DocumentBuilder builder = docFactory.newDocumentBuilder();
		this.doc = builder.parse(is);
	}

	public XmlUtils(File file) throws FileNotFoundException, ParserConfigurationException, IOException, SAXException {
		DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
		docFactory.setNamespaceAware(false); // never forget this!
		docFactory.setValidating(false);
		DocumentBuilder builder = docFactory.newDocumentBuilder();
		this.doc = builder.parse(file);
	}

	public String getXPathValue(String xPath) throws XPathExpressionException, TransformerException {
		XPathFactory factory = XPathFactory.newInstance();
		XPath xpath = factory.newXPath();
		XPathExpression exp = xpath.compile(xPath);

		NodeList nodeList = (NodeList) exp.evaluate(doc, XPathConstants.NODESET);
		return getNodeValue(nodeList);
	}

	private String getNodeValue(NodeList nodeList) throws TransformerFactoryConfigurationError, TransformerException {

		Transformer serializer = TransformerFactory.newInstance().newTransformer();
		serializer.setURIResolver(null);

		StringBuilder buf = new StringBuilder();

		for (int i = 0; i < nodeList.getLength(); i++) {
			final StringWriter sw = new StringWriter();
			serializer.transform(new DOMSource(nodeList.item(i)), new StreamResult(sw));
			String xml = sw.toString();
			final Matcher matcher = HEADER_PATTERN.matcher(xml);
			if (matcher.matches()) {
				xml = matcher.group(1);
			}

			buf.append(xml);
		}

		return buf.toString();
	}

	public List<Element> getElements(String xPath) throws XPathExpressionException {
		List<Element> elements = new ArrayList<Element>();

		XPathFactory factory = XPathFactory.newInstance();
		XPath xpath = factory.newXPath();
		XPathExpression exp = xpath.compile(xPath);

		NodeList nodes = (NodeList) exp.evaluate(doc, XPathConstants.NODESET);
		for (int i = 0; i < nodes.getLength(); i++) {
			if (nodes.item(i).getNodeType() == Node.ELEMENT_NODE) {
				elements.add((Element) nodes.item(i));
			}
		}

		return elements;
	}
	
	public static void printSOAPResponse(SOAPMessage soapResponse) throws TransformerException, SOAPException,IOException {
		TransformerFactory transformerFactory = TransformerFactory.newInstance();
		Transformer transformer = transformerFactory.newTransformer();
		transformer.setOutputProperty(OutputKeys.INDENT, "yes");
		transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "2");
		Source source = soapResponse.getSOAPPart().getContent();
		DOMResult result = new DOMResult();
		transformer.transform(source, result);
		Document document = (Document)result.getNode();
		NodeList nodes = document.getElementsByTagName("ns3:EmbeddedFile");
		for (int i = 0; i < nodes.getLength(); i++) {       
			Element EmbeddedFile = (Element)nodes.item(i);
			Element doc = (Element)EmbeddedFile.getElementsByTagName("ns3:Document").item(0);
			doc.getParentNode().removeChild(doc);          
		}	
		SOAPMessage soapResponseModified =getSoapMessageFromString(XmlUtils.documentToString(document));
		Source sourceContent = soapResponseModified.getSOAPPart().getContent();
		StreamResult resultModified = new StreamResult(System.out);
		transformer.transform(sourceContent, resultModified);	
	}
	
	public static String convertToString (SOAPMessage message) throws Exception{
		
         final StringWriter sw = new StringWriter();

         try {
             TransformerFactory.newInstance().newTransformer().transform(
                 new DOMSource(message.getSOAPPart()),
                 new StreamResult(sw));
         } catch (TransformerException e) {
             throw new RuntimeException(e);
         }
           
         return sw.toString();
		   
		  }

	public static SOAPMessage getSoapMessageFromString(String xml) throws SOAPException, IOException {
		return MessageFactory.newInstance().createMessage(new MimeHeaders(),
				new ByteArrayInputStream(xml.getBytes(Charset.forName("UTF-8"))));		
	}

	public static Document stringToXML(String xmlString)
			throws SAXException, IOException, ParserConfigurationException {
		DocumentBuilder builder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
		return builder.parse(new InputSource(new StringReader(xmlString)));
	}

	public static String documentToString(Document document)
			throws TransformerFactoryConfigurationError, TransformerException {
		DOMSource domSource = new DOMSource(document);
		StringWriter writer = new StringWriter();
		StreamResult result = new StreamResult(writer);
		Transformer transformer = TransformerFactory.newInstance().newTransformer();
		transformer.transform(domSource, result);
		return writer.toString();
	}
	
	public String documentToString()
			throws TransformerFactoryConfigurationError, TransformerException {
		DOMSource domSource = new DOMSource(doc);
		StringWriter writer = new StringWriter();
		StreamResult result = new StreamResult(writer);
		Transformer transformer = TransformerFactory.newInstance().newTransformer();
		transformer.transform(domSource, result);
		return writer.toString();
	}

	public static String createRequestResponseXML(SOAPMessage soapResponse, String fileType, String testCaseId,
			Map<String, String> disclosureInfo, String disclosureType, String operationType, String responsePath)
			throws IOException, SOAPException, TransformerException {

		String pathDivisions = "//";
		pathDivisions = System.getProperty("file.separator");

		if ("".equals(responsePath) || null == responsePath)
			responsePath = "output" + pathDivisions + "SoapAPI";

		File responseFile = new File(responsePath);
		if (!responseFile.exists()) {
			if (responseFile.mkdirs())
				ReportFactory.printAndLog(LogType.Info, "Directory is created!");
			else
				ReportFactory.printAndLog(LogType.Info, "Failed to create directory!");
		}

		StringWriter sWriter = new StringWriter();
		TransformerFactory transformerFactory = TransformerFactory.newInstance();
		Transformer transformer = transformerFactory.newTransformer();
		transformer.setOutputProperty(OutputKeys.INDENT, "yes");
		transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "2");
		Source sourceContent = soapResponse.getSOAPPart().getContent();
		StreamResult result = new StreamResult(sWriter);
		transformer.transform(sourceContent, result);

		String requestDate = new SimpleDateFormat("dd-MM-yyyy").format(Calendar.getInstance().getTime());
		File file = new File(responsePath + pathDivisions + requestDate);
		if (!file.exists()) {
			if (file.mkdirs())
				ReportFactory.printAndLog(LogType.Info, "Directory is created!");
			else
				ReportFactory.printAndLog(LogType.Info, "Failed to create directory!");
		}

		String disType = disclosureInfo.get("disType");
		String disclosureId = disclosureInfo.get("disclosureId");
		String fileName = "CA_" + fileType + "_" + disclosureType + disType + "_" + operationType + "_TestCaseId_"
				+ testCaseId + "_DisclosureId_" + disclosureId;
		FileWriter fileWriter = new FileWriter(new File(responsePath + pathDivisions + requestDate, fileName + ".xml"));
		fileWriter.write(sWriter.toString());
		fileWriter.close();
		return responsePath + pathDivisions + requestDate + pathDivisions + fileName + ".xml";
	}

	public static File getResourceAsFile(String resourcePath) {
		try {
			InputStream in = ClassLoader.getSystemClassLoader().getResourceAsStream(resourcePath);
			if (in == null) {
				return null;
			}
			File tempFile = File.createTempFile(String.valueOf(in.hashCode()), "temp.xml");
			tempFile.deleteOnExit();
			try (FileOutputStream out = new FileOutputStream(tempFile)) {
				byte[] buffer = new byte[1024];
				int bytesRead;
				while ((bytesRead = in.read(buffer)) != -1) {
					out.write(buffer, 0, bytesRead);
				}
			}
			return tempFile;
		} catch (IOException e) {
			e.printStackTrace();
			return null;
		}
	}

	public static String getConvertedString(String originalString) throws Exception {
		return new SimpleDateFormat("yyyy-mm-dd").format(new SimpleDateFormat("mm/dd/yyyy").parse(originalString));
	}

	public static SOAPMessage getResponse() {
		return soapResponse;
	}

	public static void setResponse(SOAPMessage soapMessage) {
		soapResponse = soapMessage;
	}

	public static void createXMLElement(Document document, Element elementToSet, String attributeName,
			String attributeValue) {
		Attr attributeType = document.createAttribute(attributeName);
		attributeType.setValue(attributeValue);
		elementToSet.setAttributeNode(attributeType);
	}
	
	public void createXMLElement(String elementToSet, String attributeName,
			String attributeValue)  {
		Element element = (Element) doc.getElementsByTagName(elementToSet).item(0);
		element.setAttribute(attributeName, attributeValue);
		
	}
	
	public void updateXMLElement(String elementToSet, String elementValue)  
	{
		Element element = (Element) doc.getElementsByTagName(elementToSet).item(0);
		element.setTextContent(elementValue);		
	}
	
	
	
	public String updateLoanNumberValue() throws TransformerFactoryConfigurationError, TransformerException
	{
		
		DOMSource domSource = new DOMSource(doc);
		StringWriter writer = new StringWriter();
		StreamResult result = new StreamResult(writer);
		Transformer transformer = TransformerFactory.newInstance().newTransformer();
		transformer.transform(domSource, result);
	
		return writer.toString();
	}
}
