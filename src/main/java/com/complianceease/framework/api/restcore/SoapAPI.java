package com.complianceease.framework.api.restcore;

import java.net.MalformedURLException;

import com.complianceease.framework.api.enums.ContentType;
import com.complianceease.framework.api.enums.Environment;
import com.complianceease.framework.api.enums.LogType;
import com.complianceease.framework.api.enums.Method;
import com.complianceease.framework.common.ReportFactory;
import com.complianceease.framework.common.configuration.ContextManager;
import com.complianceease.framework.common.configuration.Property;

public abstract class SoapAPI implements ISoapAPIContainer {
	protected Environment env;
	protected String endPoint;	
	protected String soapUrl;
	
	public SoapAPI(String templateEndPoint) throws MalformedURLException {
		if (templateEndPoint == null || templateEndPoint.isEmpty())
			throw new IllegalArgumentException("templateEndPoint can not be null");
		ContextManager context = ContextManager.getContext();
		String runTimeEnvName = context.getEnvironmentName();
		String runTimeEndPoint = templateEndPoint.replace("<Environment>", runTimeEnvName);
		this.setEndPoint(runTimeEndPoint);		
		ReportFactory.printAndLog(LogType.Info,String.format("<b>Request: </b></br><pre> Url: %s</br> Method: %s</br> ContentType: %s</pre>",this.soapUrl, Method.POST, ContentType.URLENC));
	}

	public SoapAPI(String templateEndPoint, Environment env) throws MalformedURLException {
		if (env == null)
			throw new IllegalArgumentException("EndPoint can not be null");
		if (templateEndPoint == null || templateEndPoint.isEmpty())
			throw new IllegalArgumentException("templateEndPoint can not be null");
		this.env = env;
		ContextManager contextManager = ContextManager.getContext();
		this.setEndPoint(String.format(templateEndPoint, contextManager.getConfigPropertyByXPath(env, Property.EndPoint)));

	}

	public SoapAPI(Environment env) throws MalformedURLException {
		ContextManager contextManager = ContextManager.getContext();
		this.setEndPoint(contextManager.getConfigPropertyByXPath(env, Property.EndPoint));		
	}

	public void setEndPoint(String endPoint) throws MalformedURLException {
		if (endPoint == null || endPoint.isEmpty())
			throw new IllegalArgumentException("EndPoint can not be null");
		this.endPoint = endPoint;
		this.soapUrl = new String(endPoint);
		System.out.println("Set webservice EndPoint : " + this.endPoint);
	}	
}
