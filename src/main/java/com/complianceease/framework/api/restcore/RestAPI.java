package com.complianceease.framework.api.restcore;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;
import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;

import com.complianceease.framework.api.enums.ContentType;
import com.complianceease.framework.api.enums.Environment;
import com.complianceease.framework.api.enums.LogType;
import com.complianceease.framework.api.enums.Method;
import com.complianceease.framework.common.ReportFactory;
import com.complianceease.framework.common.configuration.ContextManager;
import com.complianceease.framework.common.configuration.Property;


public abstract class RestAPI implements IRestAPIContainer {
	protected Environment env;
	protected String endPoint;
	protected String authKey;
	protected URL url;
	protected RestRequest httpsRequest;
	protected List<File> attachments; 
	
	public RestAPI(String templateEndPoint) throws MalformedURLException 
	{
 		if(templateEndPoint == null || templateEndPoint.isEmpty()) throw new IllegalArgumentException("templateEndPoint can not be null");
	    ContextManager context = ContextManager.getContext();
	    String runTimeEnvName = context.getEnvironmentName();
	    String runTimeEndPoint = templateEndPoint.replace("<Environment>", runTimeEnvName);
	    
	//	this.setEndPoint(String.format(templateEndPoint, contextManager.getConfigPropertyByXPath(runTimeEnvName, Property.EndPoint)));
	//	this.setAuthKey(contextManager.getConfigPropertyByXPath(runTimeEnvName, Property.WebserviceAuthKey));
	    
	    this.setEndPoint(runTimeEndPoint);
	    this.setAuthKey(context.getWebServiceAuthKey());
	    ReportFactory.printAndLog(LogType.Info, String.format(
	    		"<b>Request: </b></br><pre> Url: %s</br> Autorization: %s</br> Method: %s</br> ContentType: %s</pre>",  this.url,                                                                   
	    		this.authKey, Method.POST, ContentType.URLENC));

	}

	
	public RestAPI(String templateEndPoint, Environment env) throws MalformedURLException 
	{
		if(env == null ) throw new IllegalArgumentException("EndPoint can not be null");
		if(templateEndPoint == null || templateEndPoint.isEmpty()) throw new IllegalArgumentException("templateEndPoint can not be null");
		
		this.env = env;
		ContextManager contextManager = ContextManager.getContext();
		this.setEndPoint(String.format(templateEndPoint, contextManager.getConfigPropertyByXPath(env, Property.EndPoint)));
		this.setAuthKey(contextManager.getConfigPropertyByXPath(env, Property.WebserviceAuthKey));
	}
	
	public RestAPI(Environment env) throws MalformedURLException {
		ContextManager contextManager = ContextManager.getContext();
		this.setEndPoint(contextManager.getConfigPropertyByXPath(env, Property.EndPoint));
		this.setAuthKey(contextManager.getConfigPropertyByXPath(env, Property.WebserviceAuthKey));
	}
	
	public void setEndPoint(String endPoint) throws MalformedURLException 
	{
		if(endPoint == null || endPoint.isEmpty()) throw new IllegalArgumentException("EndPoint can not be null");
		this.endPoint = endPoint;
		this.url = new URL(endPoint);
		System.out.println("Set webservice EndPoint : " + this.endPoint);
	}
	
	public void setAuthKey(String authKey)
	{
		this.authKey = authKey;
		System.out.println("Set Webservice Key : " + this.authKey);
	}
	public void addUploadfile(File file) throws IOException {
		if(file.exists() && file.isFile()) {
			attachments.add(file);
		}
		else {
			System.err.println("Unable to locate the file : " + file.getCanonicalPath().toString());
		}
	}
	
	public void addInlineParam(String inlineParam) throws IOException {
		this.url = new URL(this.url.toString() + inlineParam);
	}
}
