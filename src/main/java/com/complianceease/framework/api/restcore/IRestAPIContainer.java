package com.complianceease.framework.api.restcore;

import java.io.IOException;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;

import org.xml.sax.SAXException;


public interface IRestAPIContainer {
	RestResponse invoke(Object... params) throws KeyManagementException, NoSuchAlgorithmException, IOException, SAXException, TransformerException, ParserConfigurationException;
}
