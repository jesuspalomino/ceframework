package com.complianceease.framework.api.restcore;

import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.net.ssl.HttpsURLConnection;
import javax.xml.soap.SOAPConnection;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPMessage;
import javax.xml.transform.TransformerException;

import org.xml.sax.SAXException;

import com.complianceease.framework.api.enums.HttpHeader;
import com.complianceease.framework.api.enums.LogType;
import com.complianceease.framework.api.util.StringUtil;
import com.complianceease.framework.api.util.XmlUtils;
import com.complianceease.framework.common.ReportFactory;

public class RestResponse {
	
	/* **************************************
	 * Sample Response Header
	 * **************************************
	 * null=[HTTP/1.1 200 OK], 
	 * Server=[Microsoft-IIS/7.5], 
	 * Last-Modified=[Tue, 05 Jan 2016 08:58:15 GMT], 
	 * Date=[Tue, 05 Jan 2016 08:58:17 GMT], 
	 * X-AspNetMvc-Version=[4.0], 
	 * Cache-Control=[public, no-store, max-age=0, s-maxage=0], 
	 * X-AspNet-Version=[4.0.30319], 
	 * CEResultCode=[SUCCESS], 
	 * Vary=[*], 
	 * Expires=[Tue, 05 Jan 2016 08:58:15 GMT], 
	 * Content-Length=[128], 
	 * X-Powered-By=[ASP.NET], 
	 * Content-Type=[application/xml; charset=utf-8]}
	 * *************************************
	 * */
	public int httpStatusCode;
	public String httpStatusResponseMessage;
	public String contentEncoding;
	public Map<String, List<String>> headers;
	public ByteArrayOutputStream body;
	public ByteArrayOutputStream bodyError;
	public List<String> errorMessages;
	private static final int BUFFER_SIZE = 4096;
	private SOAPMessage soapResponse;
	public static List<String> cookies;
	
	static {
	    //for localhost testing only
	    javax.net.ssl.HttpsURLConnection.setDefaultHostnameVerifier(
	    new javax.net.ssl.HostnameVerifier(){

	        public boolean verify(String hostname,
	                javax.net.ssl.SSLSession sslSession) {
	          /*  if (hostname.equals("localhost")) {
	                return true;
	            }*/
	            return true;
	        }
	    });
	}

	
	public RestResponse(){};
	
	public RestResponse(SOAPConnection soapConnection, String soapUrl,
			SOAPMessage commonTemplate) throws IOException, SOAPException,
			TransformerException {
		XmlUtils.printSOAPResponse(commonTemplate);
		ReportFactory.printAndLog(LogType.Info,
				"Request Submitted to Server...!");
		this.soapResponse = null;
		try {
			
			this.soapResponse = soapConnection.call(commonTemplate, soapUrl);
			ReportFactory.printAndLog(LogType.Info,
					"Response Collected from Server....!");
			XmlUtils.setResponse(this.soapResponse);
		} catch (SOAPException ex) {
			ex.printStackTrace();
		}

		// ReportFactory.printAndLog(LogType.Info,"\nResponse is :::::::\n");
		// XmlUtils.printSOAPResponse(this.soapResponse);
		soapConnection.close();
	}

	public RestResponse(int httpStatusCode, Map<String, List<String>> headers, ByteArrayOutputStream body)
	{
		this.httpStatusCode = httpStatusCode;
		this.headers = headers;
		this.body = body;
	}
	
	public RestResponse(HttpsURLConnection conn, String saveAsDir) throws IOException
	{
				
		try
		{	
			conn.connect();
			
			if(conn.getHeaderFields().get("Set-Cookie") != null)
			      cookies = conn.getHeaderFields().get("Set-Cookie");
					
			int statusCode = conn.getResponseCode();
		
			this.httpStatusCode = statusCode;
			this.httpStatusResponseMessage = conn.getResponseMessage();
			this.headers = conn.getHeaderFields();
			
			if(conn.getErrorStream() != null)
				this.bodyError = this.getResponseBodyAsByteArray(conn.getErrorStream());
			else
                this.body = this.getResponseBodyAsByteArray(conn.getInputStream());

			//download file 
			if(saveAsDir != null && saveAsDir.isEmpty() == false)
			{
				//this.downloadFile(conn, saveAsDir);
				String fileName = "fileName";
	            String disposition = conn.getHeaderField("Content-Disposition");
	            String contentType = conn.getContentType();
	            int contentLength = conn.getContentLength();
	            
	            if (disposition != null) {
	                // extracts file name from header field
	                int index = disposition.indexOf("filename=");
	                if (index > 0) {
	                    fileName = disposition.substring(index + 10,
	                            disposition.length() - 1);
	                }
	            } 
	            
	            ReportFactory.printAndLog(LogType.Info, "Content-Type = " + contentType);
	            ReportFactory.printAndLog(LogType.Info, "Content-Disposition = " + disposition);
	            ReportFactory.printAndLog(LogType.Info, "Content-Length = " + contentLength);
	            ReportFactory.printAndLog(LogType.Info, "fileName = " + fileName);
	
	            String saveFilePath = saveAsDir + "/" + fileName;
	            FileOutputStream outputStream = new FileOutputStream(saveFilePath);
	            this.body.writeTo(outputStream); 
	            outputStream.close();
			}
			
			if(this.body != null)
			    this.body.close();
			
			if(this.bodyError != null)
				this.bodyError.close();
		
		}
		catch (Exception ex) {
			this.addErrorMessage(message("Failure reading error stream: "+ ex.getMessage() + ex.getStackTrace()));
		}
		finally {
			conn.disconnect();
		}
	}

	public void saveOutputFileAs(String saveAs)
            throws IOException {
            FileOutputStream outputStream = new FileOutputStream(saveAs);
            this.body.writeTo(outputStream); 
            outputStream.close();
            this.body.close();
            System.out.println("File downloaded.");
    }
	
    private String message(String msg) {
    	return (msg == null || msg.isEmpty())? "" : "["+new Date(System.currentTimeMillis())+"] "+msg;
    }
    
    public RestResponse handleException(HttpsURLConnection conn, Exception ex) throws IOException, SAXException {    	
    	
    	RestResponse response = new RestResponse();
    	
    	response.addErrorMessage(message("ERROR: "+ ex.getMessage()));

    	String errMessage = null;
    	switch(conn.getResponseCode())
    	{
			case HttpsURLConnection.HTTP_MOVED_TEMP :
				errMessage = "Invalid request, web service not found";
			break;
			case HttpsURLConnection.HTTP_BAD_REQUEST :
				errMessage = "Invalid request, web service not found";
			break;
    		case HttpsURLConnection.HTTP_NOT_FOUND :
    			errMessage = "Invalid request, web service not found";
    			break;
    		case HttpsURLConnection.HTTP_UNAUTHORIZED :
    			errMessage = "User is not authorized to perform this action";
    			break;
    		case HttpsURLConnection.HTTP_FORBIDDEN :
    			errMessage = "User is not authorized to perform this action";
    			break;
    	}

    	response.addErrorMessage(message(errMessage));
       
    	// Read the Error Message
    	try {
    		
        	response.httpStatusCode =  conn.getResponseCode();
        	response.headers = conn.getHeaderFields();
    		response.body = getResponseBodyAsByteArray(conn.getErrorStream());
    	} 
    	catch (Exception e) {
    		response.addErrorMessage("Failure reading error stream: "+ e.getMessage() + e.getStackTrace());
    	}
       
    	return response;
    }
    
	public void addErrorMessage(String message) {
		if(errorMessages == null)
			this.errorMessages = new ArrayList<String>();
		
		errorMessages.add(message);
	}
	
	public String getBodyAsString() {
		
		System.out.println(body.toString());
		ReportFactory.printAndLog(LogType.Info,"<b>Response: </b></br><pre>"+ body.toString().replace("<", "&lt;") +"</pre>" );

		return body.toString();
	}
	
    public String getBodyErrorAsString() {
		
		System.out.println(bodyError.toString());
		ReportFactory.printAndLog(LogType.Info,"<b>Response: </b></br><pre>"+ bodyError.toString().replace("<", "&lt;") +"</pre>" );

		return bodyError.toString();
	}
	
	public String getSoapBodyAsString() {
		System.out.println(body.toString());
		return body.toString();
	}
	
	public void showAllHeaders() {
		for(Map.Entry<String, List<String>> header : headers.entrySet())
		{
			System.out.println(header.getKey() + "=" + header.getValue().toString());
		}
	}
	
	public String getHttpType(){
		
		return headers.get(HttpHeader.HttpStatusCode).get(0).split("\\s+", 3)[0];
	}
	
	public int getHttpStatusCode(){
		return Integer.parseInt(headers.get(HttpHeader.HttpStatusCode).get(0).split("\\s+", 3)[1]);
	}
	
	public String getHttpStatusMessage(){
		return headers.get(HttpHeader.HttpStatusCode).get(0).split("\\s+", 3)[2];
	}

	public String getHttpStatus(){
		return headers.get(HttpHeader.HttpStatusCode).get(0);
	}

	public String getCEResultCode(){
		
		return headers.get(HttpHeader.CEResultCode).get(0);
	}

	public String getCacheControl(){
		
		return headers.get(HttpHeader.CacheControl).get(0);
	}

	public String getContentType(){
		
		return headers.get(HttpHeader.ContentType).get(0);
	}

	public String getContentEncoding(){
		
		return headers.get(HttpHeader.ContentType).get(0).split("=", 2)[1];
	}

	public String getContentLength(){
		
		return headers.get(HttpHeader.ContentLength).get(0);
	}

	public String getExpires(){
		
		return headers.get(HttpHeader.Expires).get(0);
	}

	public String getLastModified(){
		
		return headers.get(HttpHeader.LastModified).get(0);
	}

	public String getVary(){

		return headers.get(HttpHeader.Vary).get(0);
	}

	public String getServer(){
		
		return headers.get(HttpHeader.Server).get(0);
	}

	public String getPoweredBy(){
		
		return headers.get(HttpHeader.PoweredBy).get(0);
	}

	public String getResponseDateTime(){
		
		return headers.get(HttpHeader.ResponseDateTime).get(0);
	}

	public String getAspNetMvcVersion(){
		
		return headers.get(HttpHeader.XAspNetMvcVersion).get(0);
	}

	public String getAspNetVersio(){
		
		return headers.get(HttpHeader.XAspNetVersion).get(0);
	}

	private ByteArrayOutputStream getResponseBodyAsByteArray(InputStream inputStream)
			throws IOException {

		DataInputStream input = new DataInputStream(inputStream);
		
		ByteArrayOutputStream output = new ByteArrayOutputStream();
		
		int bufferSize = 1204;
		byte[] buffer = new byte[bufferSize];
		int bytesRead = input.read(buffer, 0, bufferSize);
		
		while (bytesRead > 0) {
			output.write(buffer, 0, bytesRead);
			bytesRead = input.read(buffer, 0, bufferSize);
		}
		
		input.close();
		output.flush();
		output.close();
		
		return output;
	}
}