package com.complianceease.framework.api.restcore;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;

import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPMessage;
import javax.xml.transform.TransformerException;

import org.xml.sax.SAXException;

public interface ISoapAPIContainer {
	RestResponse soapInvoke(SOAPMessage commonTemplate) throws FileNotFoundException, KeyManagementException,
			NoSuchAlgorithmException, IOException, SAXException, SOAPException, TransformerException;
}
