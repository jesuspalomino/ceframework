package com.complianceease.framework.api.restcore;

import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.net.URLEncoder;
import java.security.KeyManagementException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.KeyManagerFactory;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.soap.SOAPConnection;
import javax.xml.soap.SOAPConnectionFactory;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPMessage;
import javax.xml.transform.TransformerException;
import org.xml.sax.SAXException;

import com.complianceease.framework.api.enums.ContentType;
import com.complianceease.framework.api.enums.DocumentType;
import com.complianceease.framework.api.enums.LogType;
import com.complianceease.framework.api.enums.Method;
import com.complianceease.framework.api.util.Core;
import com.complianceease.framework.api.util.ExcelUtils;
import com.complianceease.framework.api.util.XmlUtils;
import com.complianceease.framework.common.ReportFactory;
import com.google.common.base.Joiner;

public class RestRequest {

	public URL url = null;
	public Method method = Method.POST;
	public ContentType contentType = ContentType.MULTIPART;
	private HttpsURLConnection httpsConn = null;
	private DataOutputStream dataOutputStream = null;
	private String boundary = null;
	public final String charset = "UTF-8";
	private final String newLine = "\r\n";
	private final String twoHyphens = "--";
	//private final int maxBufferSize = 1024 * 1024;
	public List<File> attachments; 
	public Map<String, String> headers;
	public String saveAsDir = null;
	private SOAPConnection soapConnection=null;
	private String soapUrl=null;
	
	public RestRequest(String soapUrl) throws SOAPException {
		this.soapConnection = SOAPConnectionFactory.newInstance().createConnection();
		this.soapUrl=soapUrl;
	}
		
	public RestRequest(URL url, File certificate, String password) {

		try {
			
			this.addCustomCertificate(password, certificate);
			this.setUrlConnection(url);
			
		} catch (UnrecoverableKeyException | KeyManagementException | KeyStoreException | NoSuchAlgorithmException
				| CertificateException | IOException e) {
		
			ReportFactory.printAndLog(LogType.Error,e.getMessage());
		}

	}
	
	public RestRequest(String templateEndPoint, Map<String,String> param) throws KeyManagementException, NoSuchAlgorithmException, IOException 
	{	
		String parameters = setUrlQueryParametersForCLL(param);
		String webserviceUrl = templateEndPoint + parameters;
		ReportFactory.printAndLog(LogType.Info, "Invoking REST API : " + webserviceUrl);
		URL URL = new URL(webserviceUrl);
		this.trustingAllCert();
		this.setUrlConnection(URL);
	}
	
	public RestRequest(URL url) throws KeyManagementException, NoSuchAlgorithmException, IOException {

		this.trustingAllCert();
		this.setUrlConnection(url);
	}

	public RestRequest(URL url, Method method, Map<String, String> headers, ContentType contenType) 
			throws IOException,
			NoSuchAlgorithmException,
			KeyManagementException
	{
		//default 
		this.trustingAllCert();
        this.setUrlConnection(url);
    	this.setRequestMethod(method);
    	this.setRequestHeaders(headers);
    	this.setContentType(contenType);
	}
	
	private void buildHttpsURLConnection() 
			throws ProtocolException, 
			IOException
	{
        if (RestResponse.cookies != null)
            this.httpsConn.addRequestProperty("Cookie", RestResponse.cookies.get(0));


    	this.dataOutputStream = new DataOutputStream(this.httpsConn.getOutputStream());
	}
	
	private void trustingAllCert() throws NoSuchAlgorithmException, KeyManagementException {
    	TrustManager[] trustAllCerts = new TrustManager[]{
    			new X509TrustManager() {
                       public X509Certificate[] getAcceptedIssuers() { return null; };

                       public void checkServerTrusted(X509Certificate[] certs, String authType) throws CertificateException {};

                       public void checkClientTrusted(X509Certificate[] certs, String authType) throws CertificateException {};
    			}
    	};

        SSLContext sc = SSLContext.getInstance("SSL");
        sc.init(null, trustAllCerts, new SecureRandom());
        HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
        
        HostnameVerifier hv = new HostnameVerifier() {
        	public boolean verify(String urlHostName, SSLSession session) {
               if (!urlHostName.equalsIgnoreCase(session.getPeerHost())) {
                   System.out.println("Warning: URL host '" + urlHostName + "' is different to SSLSession host '" + session.getPeerHost() + "'.");
               }
               return true;
        	}
        };
       
       HttpsURLConnection.setDefaultHostnameVerifier(hv);
    }
	
	private void addCustomCertificate(String password, File filePath) throws KeyStoreException, NoSuchAlgorithmException, 
	CertificateException, IOException, UnrecoverableKeyException, KeyManagementException {
		
		KeyStore keyStore = KeyStore.getInstance("PKCS12");
		FileInputStream certificatePath = new FileInputStream(filePath);
		keyStore.load(certificatePath, password.toCharArray());
		KeyManagerFactory kmf = KeyManagerFactory.getInstance("SunX509");
		kmf.init(keyStore, password.toCharArray());
		SSLContext sc = SSLContext.getInstance("SSL");
		sc.init(kmf.getKeyManagers(), null, null);
		
	    HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
    	
    }
	
	public void setUrlConnection(URL url) 
			throws MalformedURLException, 
			IOException
	{
		this.url = url;
		this.httpsConn = (HttpsURLConnection) this.url.openConnection();
		this.httpsConn.setDoOutput(true);
		this.httpsConn.setDoInput(true); //Allow Inputs
		this.httpsConn.setUseCaches(false); // Don't use a Cached Copy
		this.httpsConn.setRequestProperty("Connection", "Keep-Alive");
		this.httpsConn.setRequestProperty("Accept-Charset", this.charset);
		//this.httpsConn.setRequestProperty("Accept-Encoding", "gzip,deflate");
		this.httpsConn.setInstanceFollowRedirects(false);  //you still need to handle redirect manully.
		HttpsURLConnection.setFollowRedirects(false);


		//     this.httpsConn.setRequestProperty("Accept", ""); 
		//     this.httpsConn.setRequestProperty("Content-Length", fileSize);
		//	   this.httpsConn.setChunkedStreamingMode(94825); 
		//cannot write to a URLConnection if doOutput=false
	}
	
	public void addHeader(String key, String value)
	{
		if(key.isEmpty())
			throw new NullPointerException("key : " + key);
		if(value.isEmpty()) 
			throw new NullPointerException("value : " + value);

		//this.headers.put(key, value);
		
	   this.httpsConn.setRequestProperty(key, value);
	
	}
	
	public void addBasicAuthorization(String userName, String password)
	{
		String userPassword = userName + ":" + password;
		@SuppressWarnings("restriction")
		String encoding = new sun.misc.BASE64Encoder().encode(userPassword.getBytes());
		this.httpsConn.setRequestProperty("Authorization", "Basic " + encoding);
	}
	
	public URL getUrl()
	{
		return this.url;
	}
	public Method getMethod()
	{
		return this.method;
	}
	public Map<String, String> getHeaders()
	{
		return this.headers;
	}
	
	public void setRequestMethod(Method method) throws ProtocolException
	{
		this.method = method;
		this.httpsConn.setRequestMethod(method.name());
	}
	
	public void setRequestHeaders(Map<String, String> headers) 
	{
		for (Map.Entry<String, String> pair : headers.entrySet()) {
			this.httpsConn.setRequestProperty(pair.getKey(), pair.getValue());
		}
	}
	
	public void setContentType(ContentType contentType) 
	{
    	this.contentType = contentType;
    	this.boundary = "***" + new Timestamp(System.currentTimeMillis()) + "***";
    	this.httpsConn.setRequestProperty("Content-Type", contentType.toString() + "; boundary=\"" + this.boundary +"\"");
	}
	
	public void setContentType(String contentType) 
	{
    	this.boundary = "***" + new Timestamp(System.currentTimeMillis()) + "***";
    	this.httpsConn.setRequestProperty("Content-Type", contentType + "; boundary=\"" + this.boundary +"\"");
	}
	
	public void setMultiPartFormData(Map<String, String> parameters) 
			throws FileNotFoundException, 
			IOException  
	{
        for(DocumentType fileType : DocumentType.values())
        {
               String fileTypeName = fileType.name();
               
               if(parameters.containsKey(fileTypeName))
               {
                     if (fileTypeName.equals("base64Transcripts")) {
                            
                            String value = parameters.get(fileTypeName).trim();
                            parameters.put(fileTypeName, Core.convertToBase64String(value));  
                     }
                     else {
                            this.addUploadFile(new File(parameters.get(fileTypeName)));
                            parameters.remove(fileTypeName);
                     }     
               }
        }

		
		//Add form-data parameters
		this.setMultiPartParameters(parameters);
	}
	
	public void addUploadFile(File file) 
			throws FileNotFoundException, 
			IOException  
	{
		addAttachFile(file, null);
	}
	
	public void addUploadFile(File file, String paremeterName) 
			throws FileNotFoundException, 
			IOException  
	{
		addAttachFile(file, paremeterName);
	}
	
	
	private void setMultiPartParameters(Map<String, String> parameters) throws IOException
	{
    	for(Map.Entry<String, String> entry : parameters.entrySet()) {
    		this.addMultiPartParameter(entry.getKey(), entry.getValue());
    	}
    	
    	if(parameters.size()>0)
    		this.dataOutputStream.writeBytes(this.twoHyphens + this.boundary + this.twoHyphens);
    	
/*		Iterator<Entry<String, String>> it = parameters.entrySet().iterator();
		
        while (it.hasNext()) {

			Map.Entry<String, String> pair = (Map.Entry<String, String>)it.next();
    		this.addMultiPartParameter(pair.getKey(), pair.getValue());
        	it.remove(); // avoids a ConcurrentModificationException
        }
*/       
	}
	
	private void addMultiPartParameter(String key, String value) throws IOException
	{
		this.dataOutputStream.writeBytes(this.twoHyphens + this.boundary + this.newLine);
		this.dataOutputStream.writeBytes("Content-Disposition: form-data; name=\"" + key + "\"" + this.newLine);
		this.dataOutputStream.writeBytes(this.newLine);
		this.dataOutputStream.writeBytes(value);
		this.dataOutputStream.writeBytes(this.newLine);
	}
	
	public  String setUrlQueryParametersForCLL(Map<String,String> params) throws IOException
	{
       Iterator<Entry<String, String>> it = params.entrySet().iterator();
       String result = "?";
       
        while (it.hasNext()) {
        	Map.Entry<String, String> pair = (Map.Entry<String, String>)it.next();
        	
        	String key = pair.getKey().trim();
        	String value = pair.getValue().trim();
        
            if (!key.equals("TestCaseId") && !key.equals("TestCaseName")&& !key.equals("Expected")&& !key.equals("UserName"))
            {
            	 result += (URLEncoder.encode(key, charset));
            	 result += "=";
            	 result += (URLEncoder.encode(value, charset));
			     result+="&";
            } 
        }
        
        ReportFactory.printAndLog(LogType.Info, "<b>Request: </b></br><pre>" + result.substring(0,result.length()-1) +"</pre>");
		return result.substring(0,result.length()-1);
	}

	public void setUrlQueryParameters(Map<String, String> parameters) throws UnsupportedEncodingException, IOException
	{
		Boolean first = true;
		
		StringBuilder result = new StringBuilder();

		for(DocumentType fileType : DocumentType.values())
		{
			String fileTypeName = fileType.name();
			
			if(parameters.containsKey(fileTypeName))
			{
				String value = parameters.get(fileTypeName).trim();
				
				if(value.isEmpty() == false && value.equals(null) == false)
				{
					if (first)
						first = false;
					else
						result.append("&");
	            
					result.append(URLEncoder.encode(fileTypeName, charset));
					result.append("=");
					result.append(URLEncoder.encode(Core.convertToBase64String(value), charset));  
					parameters.remove(fileTypeName);
				}
			}
		}

		Iterator<Entry<String, String>> it = parameters.entrySet().iterator();
		
        while (it.hasNext()) {
        	Map.Entry<String, String> pair = (Map.Entry<String, String>)it.next();
        	
            if (first)
                first = false;
            else
                result.append("&");

            result.append(URLEncoder.encode(pair.getKey().trim(), charset));
            result.append("=");
            result.append(URLEncoder.encode(pair.getValue().trim(), charset));
        }

        this.dataOutputStream.writeBytes(result.toString());
	}
	
	public RestResponse invoke(Object[] params) throws IOException, ParserConfigurationException, TransformerException, SAXException
	{
		Map<String, String> parameters = this.getParameters(params);
		
		return this.invoke(parameters);
	}
	
	public RestResponse invoke() throws IOException
	{
//		if(this.dataOutputStream == null)
//			this.buildHttpsURLConnection();
		
		
		return this.execute();
	}
	
	public RestResponse invoke(Map<String, String> parameters) throws FileNotFoundException, IOException, SAXException, TransformerException, ParserConfigurationException
	{	
		
		if(this.dataOutputStream == null)
			this.buildHttpsURLConnection();
		
		if(this.contentType == ContentType.URLENC)
		{
			this.setUrlQueryParameters(parameters);
		}
		else if (this.contentType == ContentType.XML) 
		{
			this.setXmlParameters(parameters, new FileInputStream (new File ("../qvp/src/main/java/com/complianceease/qvp/dataprovider/ReIssue_Response_Template.xml")));
		}
		else
		{
			this.setMultiPartFormData(parameters); 
		}
		
		return this.execute();
	}
	
	// set reportid, identity attributes to XML
	public void setXmlParameters(Map <String, String> parameters, InputStream os) throws IOException, SAXException, TransformerException, ParserConfigurationException
	{
		XmlUtils util = new XmlUtils(os);
		util.createXMLElement("Reissue", "ReportID", parameters.get("ReportID") );
		util.createXMLElement("Reissue", "Identity", parameters.get("Identity") );
		this.dataOutputStream.writeBytes(util.documentToString());
	}
	
	public RestResponse invoke(File file) throws IOException
	{
		this.addUploadFile(file);
		return this.execute();
	}
	
	public  RestResponse invoke(String key, String[] values) throws IOException
	{
		if(this.dataOutputStream == null)
			this.buildHttpsURLConnection();
		
		StringBuilder result = new StringBuilder();
        result.append(URLEncoder.encode(key, charset));
        result.append("=");
        Joiner joiner = Joiner.on(",");
        result.append(URLEncoder.encode(joiner.join(values), charset));

        this.dataOutputStream.writeBytes(result.toString());

		return this.execute();
	}
	
	public RestResponse soapInvoke(SOAPMessage commonTemplate) throws IOException, SOAPException, TransformerException {
		return this.soapExecute(commonTemplate);
	}
	
	private RestResponse soapExecute(SOAPMessage commonTemplate) throws IOException, SOAPException, TransformerException {		
		RestResponse response = new RestResponse(this.soapConnection,this.soapUrl,commonTemplate);
		return response;
	}

	private RestResponse execute() throws IOException {

		if(this.dataOutputStream != null)
		{
			this.dataOutputStream.flush();	
			this.dataOutputStream.close();
		}
		
		RestResponse response =  new RestResponse(this.httpsConn, this.saveAsDir);
		return response;
	}

    private Map<String, String> getParameters(Object[] testData) throws IOException
	{	
    	Map<String, String> parameters = new HashMap<String, String>();

    	parameters = ExcelUtils.getParameters(testData);
		
		if(parameters.containsKey("SaveAsDir"))
			this.saveAsDir = parameters.get("SaveAsDir");

		return parameters;
	}
   

	private void addAttachFile(File file, String parameterName) throws ProtocolException, IOException  
    {
    	if(this.dataOutputStream == null)
			this.buildHttpsURLConnection();
		
		int bytesRead, bytesAvailable, bufferSize;
		byte[] buffer;
		
		FileInputStream fileInputStream = new FileInputStream(file);
	
		
		if(this.contentType == ContentType.MULTIPART)
		{
			String name = parameterName != null ? parameterName: "attachment";
			this.dataOutputStream.writeBytes(this.twoHyphens + this.boundary + this.newLine);		
			this.dataOutputStream.writeBytes("Content-Disposition: form-data; name=\""+name+"\";" + " filename=\"" + file.getName() + "\"" + this.newLine);		
			this.dataOutputStream.writeBytes(this.newLine);
		}

		bytesAvailable = fileInputStream.available();
		bufferSize = Math.min(bytesAvailable, 1024);
		buffer = new byte[bufferSize];
		bytesRead = fileInputStream.read(buffer, 0, bufferSize);

		while (bytesRead > 0) {
			this.dataOutputStream.write(buffer, 0, bufferSize);
			bytesAvailable = fileInputStream.available();
			bufferSize = Math.min(bytesAvailable, 1024);
			bytesRead = fileInputStream.read(buffer, 0, bufferSize);
		}
		this.dataOutputStream.writeBytes(this.newLine);
		fileInputStream.close();
		
		if(this.attachments == null)
			attachments = new ArrayList<File>();
		
		this.attachments.add(file);
    }

}
