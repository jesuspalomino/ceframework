package com.complianceease.framework.util;

import java.sql.SQLException;

import com.complianceease.framework.common.configuration.ContextManager;
import com.complianceease.framework.common.models.LoginAccount;
import com.complianceease.framework.database.AccountType;
import com.complianceease.framework.database.QueriesRepository;

public class QueryUtil {
	

	public static LoginAccount getAccountInfo(AccountType accountType)
	{
		LoginAccount loginAccount = new LoginAccount();
		
		try {
			
			loginAccount =  QueriesRepository.FindCredentials(accountType.getValue(), ContextManager.getContext().getCurrentEnvironment());
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return loginAccount;
	}
	
	public static LoginAccount getAccountInfo(String userName)
	{
		LoginAccount loginAccount = new LoginAccount();
		
		try {
			
			loginAccount =  QueriesRepository.getCredentialsByUserName(userName, ContextManager.getContext().getCurrentEnvironment());
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return loginAccount;
	}

}
