package com.complianceease.framework.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import com.complianceease.framework.api.enums.LogType;
import com.complianceease.framework.common.ReportFactory;

public class ZipUtil {


	public static String unZipIt(String zipFile, String outputFolder)
	{
		byte[] buffer = new byte[1024];
		String fileunZipPath ="";

		try{

			//create output directory is not exists
			File folder = new File(outputFolder);
			
			if(!folder.exists()){
				folder.mkdir();
			}

			//get the zip file content
			ZipInputStream zis = new ZipInputStream(new FileInputStream(zipFile));
			//get the zipped file list entry
			ZipEntry ze = zis.getNextEntry();

			while(ze!=null){

				String fileName = ze.getName();
				File newFile = new File(outputFolder + File.separator + fileName);
				fileunZipPath = newFile.getAbsoluteFile().toString();
				ReportFactory.printAndLog(LogType.Info, "<b>File Unzip : </b>"+ newFile.getAbsoluteFile());

				//create all non exists folders
				//else you will hit FileNotFoundException for compressed folder
				new File(newFile.getParent()).mkdirs();

				FileOutputStream fos = new FileOutputStream(newFile);

				int len;
				while ((len = zis.read(buffer)) > 0) {
					fos.write(buffer, 0, len);
				}

				fos.close();
				ze = zis.getNextEntry();
			}

			zis.closeEntry();
			zis.close();


		}catch(IOException ex){
			ex.printStackTrace();
			ReportFactory.printAndLog(LogType.Error, ex.getMessage());
			
		}
		

		return fileunZipPath;
	}

}

