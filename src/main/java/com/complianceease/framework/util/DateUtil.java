/*
 * $Id: SecurityManagerTest.java 10607 2012-07-18 05:30:54Z kwimmer $
 *
 * Copyright (c) 2016 LogicEase Solutions Inc.
 * One Bay Plaza Suite LL33
 * 1350 Old Bayshore Highway
 * Burlingame, California 94010, U.S.A.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * LogicEase Solutions Inc. ("Confidential Information").  You shall
 * not disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with LogicEase Solutions.
 */

package com.complianceease.framework.util;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * JUnit test case for the {@link SecurityManager} class.
 *
 * @version $Revision: 10607 $ $Date: 2012-07-17 22:30:54 -0700 (Tue, 17 Jul 2012) $
 * @author David Chan
 */
public class DateUtil {

    public static String getDateTime() {
        DateFormat df = new SimpleDateFormat("MMddyyhhmmss");
        Date today = Calendar.getInstance().getTime();
        String sDateTimeStamp = df.format(today);
        return sDateTimeStamp;
    }

}
