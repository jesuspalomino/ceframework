package com.complianceease.framework.util;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.PdfReader;
import com.itextpdf.text.pdf.parser.FilteredTextRenderListener;
import com.itextpdf.text.pdf.parser.LocationTextExtractionStrategy;
import com.itextpdf.text.pdf.parser.PdfReaderContentParser;
import com.itextpdf.text.pdf.parser.PdfTextExtractor;
import com.itextpdf.text.pdf.parser.RegionTextRenderFilter;
import com.itextpdf.text.pdf.parser.RenderFilter;
import com.itextpdf.text.pdf.parser.SimpleTextExtractionStrategy;
import com.itextpdf.text.pdf.parser.TextExtractionStrategy;

public class PdfDocumentUtil {
	

   public static String extractTextFromPdf(String pathFile)
   {
	    String text = "";
		PdfReader reader;
		
		try {
			
			reader = new PdfReader(pathFile);
			PdfReaderContentParser parser = new PdfReaderContentParser(reader);
			TextExtractionStrategy strategy = null;
			
			for(int i = 1; i <= reader.getNumberOfPages(); i++) {

			    strategy = parser.processContent(i,new SimpleTextExtractionStrategy());		   
			    String textByPage = strategy.getResultantText();
			    text += textByPage; 
			}
			
		} catch (IOException e) {
			System.out.println(e.getMessage());
		}
		
		return text;
   }
   
   public static Map<String, String> getTextByFields(String file, String formType)
   {
	    JSONParser parser = new JSONParser(); 
		HashMap<String,String> map = new HashMap<String, String>();
		formType = formType.toUpperCase();
		
        try {
			Object obj = getObject(formType);
			JSONObject jsonObject = (JSONObject) obj;  
						
			 for(Object key : jsonObject.keySet())
			 {
				 String keyStr = (String)key;
				 
				 if(keyStr.toUpperCase().equals(formType))
				 {
					 JSONObject objPages = (JSONObject) jsonObject.get(key);
					 
					 for(Object page :objPages.keySet())
			    	 {
						 String pageName = (String)page;
						 JSONObject objFields = (JSONObject) objPages.get(page);
						  
						 for(Object field:objFields.keySet())
						 {
							 String fieldName = (String)field;
							 String value =  objFields.get(fieldName).toString();
							 String[] aValue = value.split(" ");
							 int pageNumber = Integer.parseInt(pageName);
							 String fieldValue = getTextSpecificField(file,aValue,pageNumber);
							 map.put(fieldName, fieldValue);
						 }
			    	 } 
				 }
			 }
		
    	} catch (IOException e) {
			System.out.println(e.getMessage());
		}
				
		return map;
   }
   
    private static String getTextSpecificField(String pdf, String[] position, int pageNumber) throws IOException
	{
		String text = "";
		int llx = Integer.parseInt(position[0]);
		int lly = Integer.parseInt(position[1]);
		int urx = Integer.parseInt(position[2]);
		int ury = Integer.parseInt(position[3]);
		
		PdfReader reader = new PdfReader(pdf);
		Rectangle rect = new Rectangle(llx, lly, urx, ury);
		RenderFilter filter = new RegionTextRenderFilter(rect);
		TextExtractionStrategy strategy = null;
		strategy = new FilteredTextRenderListener(new LocationTextExtractionStrategy(),filter);
		text = PdfTextExtractor.getTextFromPage(reader, pageNumber, strategy);

		return text;
	}
    
    private static Object getObject(String formType)
    {
    	JSONParser parser = new JSONParser(); 
    	Object obj = null;
    	String file= "";
    	String firstLetter = formType = formType.toUpperCase().substring(0, 1);
    	 
		try {
			
			if(firstLetter.matches("[B-M]"))
	    		file = "B-M-MappingFieldPosition.json";
	    	else if(firstLetter.matches("[N-Z]"))
	    		file = "N-Z-MappingFieldPosition.json";
	    	else
	    		file = "A-MappingFieldPosition.json";
			
			obj = parser.parse(new FileReader("./src/main/java/com/complianceease/cdp/dataproviders/"+file));
			
		} catch (FileNotFoundException e) {
			System.out.println(e.getMessage());
		} catch (IOException e) {
			System.out.println(e.getMessage());
		} catch (ParseException e) {
			System.out.println(e.getMessage());
		}
		
    	return obj;
    	
    }

}
   
   