package com.complianceease.framework.util;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;

import com.google.common.io.Files;

public class FileUtil {

	public static void deleteFilesInDirectory(File file) {

		
		if (file.exists()) {
			
			File[] contents = file.listFiles();

			for (int i = 0; i < contents.length; i++) {
				if (contents[i].isDirectory()) {
					System.out.println("Deleting Directory :" + contents[i].getAbsolutePath());
					try {
						FileUtils.deleteDirectory(new File(contents[i].getAbsolutePath())); // deletes the whole folder
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				} else {
					System.out.println("Deleting File :" + file.getAbsolutePath());
					// it is a simple file. Proceed for deletion
					contents[i].delete();
				}
			}
		}

	}

}
