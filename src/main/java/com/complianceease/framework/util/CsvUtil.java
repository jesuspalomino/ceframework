package com.complianceease.framework.util;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import com.complianceease.framework.api.enums.LogType;
import com.complianceease.framework.common.ReportFactory;
import com.google.common.collect.Iterables;

import au.com.bytecode.opencsv.*;

public class CsvUtil {

	/**
	 * Method to get data from .csv file
	 * 
	 * @param csvReader
	 * @return
	 * @throws IOException
	 */
	public static List<String[]> readDataFromCsvFile(CSVReader csvReader) throws IOException {
		return csvReader.readAll();
	}

	public static int getHeaderLocation(String[] headers, String columnName) {

		return Arrays.asList(headers).indexOf(columnName);
	}

	public static String[] getCsvHeader(String input)
	{
		CSVReader reader;
		List<String[]> csvBody = null;

		try {

			reader = new CSVReader(new FileReader(input));
			csvBody = reader.readAll();

		} catch (FileNotFoundException e) {
			ReportFactory.printAndLog(LogType.Error, e.getMessage());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			ReportFactory.printAndLog(LogType.Error, e.getMessage());
		}

		return csvBody.get(0);

	}

	//Get the value of all rows of a specific Column name.
	public static List<String> getValuesByHeader(File input, String columnName)
	{
		
		List<String> list = new ArrayList<String>();
		
		try {
			
			CSVReader csvReader = new CSVReader(new FileReader(input));
			List<String[]> csvBody = csvReader.readAll();
			int columnIndex = getHeaderLocation(csvBody.get(0), columnName);
			csvReader.close();
			
			csvBody.forEach(item -> {
				
				list.add(item[columnIndex]);
			});
			
			list.remove(0);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			ReportFactory.printAndLog(LogType.Error, e.getMessage());
		}
	
		return list;
		
	}

	public static void updateCSV(String input, String output, String replace, int col) throws IOException {   

		CSVReader reader = new CSVReader(new FileReader(input));
		List<String[]> csvBody = reader.readAll();

		for(String[] item: Iterables.skip(csvBody, 1))
		{
			item[col] = replace;
		}
		
		reader.close();

		CSVWriter writer = new CSVWriter(new FileWriter(output));
		writer.writeAll(csvBody);
		writer.flush();
		writer.close();
	}
	
	public static void updateCSV(String input, String output, Map<Integer,String> values) throws IOException {   

		CSVReader reader = new CSVReader(new FileReader(input));
		List<String[]> csvBody = reader.readAll();

		for(String[] item: Iterables.skip(csvBody, 1))
		{
			values.forEach((key,value) ->{
				item[key] = value;
			});		
		}
		
		reader.close();

		CSVWriter writer = new CSVWriter(new FileWriter(output));
		writer.writeAll(csvBody);
		writer.flush();
		writer.close();
	}

}
