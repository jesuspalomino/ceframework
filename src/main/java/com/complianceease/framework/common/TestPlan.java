package com.complianceease.framework.common;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.MalformedURLException;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.xpath.XPathExpressionException;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.testng.Assert;
import org.testng.ITestContext;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import org.xml.sax.SAXException;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.complianceease.framework.api.enums.Environment;
import com.complianceease.framework.api.enums.LogType;
import com.complianceease.framework.api.restcore.RestResponse;
import com.complianceease.framework.api.util.ExcelUtils;
import com.complianceease.framework.api.util.StringUtil;
import com.complianceease.framework.api.util.XmlUtils;
import com.complianceease.framework.common.configuration.ContextManager;
import com.complianceease.framework.database.PoolManager;
import com.complianceease.framework.util.FileUtil;
import com.complianceease.framework.web.util.WebUIDriver;

public class TestPlan {

	public String testPlanName;
	public static Environment Env;
	protected ExtentReports reporter;
	protected ExtentTest logger;
	private static String testName;
	private ArrayList<Object[]> testDataSet;
	public String createdByGid;
	public static String responsePath;
	public static String dataProviderPath;
	private Boolean runSmokeTests = false;
	private Boolean priorityP0 = false;
	private Boolean priorityP1 = false;
	private int dataProviderSize;

	public int getDataProviderSize() {
		return dataProviderSize;
	}

	public void setDataProviderSize(int dataProviderSize) {
		this.dataProviderSize = dataProviderSize;
	}

	public static File testFileName;
	public static String testMethodName;

	public TestPlan() {
		System.out.println("Constructing an instance of TestPlan Class");
	}

	public File getTestDataFile(File testDataFile) throws IOException {

		if (testDataFile.exists())
			return testDataFile;
		else
			throw new IOException("Unable to find the test file" + testDataFile.getAbsolutePath().toString());
	}

	public List<Object[]> getTestDataFromExcel(File testFile, String testMethod)
			throws IOException, EncryptedDocumentException, InvalidFormatException {
		// File file = getTestDataFile(testFile);
		testFileName = testFile;
		testMethodName = testMethod;

		ExcelUtils excelUtils = new ExcelUtils(testFile);

		testDataSet = new ArrayList<Object[]>();

		try {

			int lastRow = excelUtils.getLastRowNum();

			for (int i = 0; i <= lastRow; i++) {
				if (runSmokeTests) {

					if (excelUtils.getCellData(i, 1).equals(testMethod)
							&& excelUtils.getCellData(i, 2).toUpperCase().equals("Y")) {

						System.out.println("Loading: ExcelWorkSheet - Row # " + i);
						testDataSet.add(excelUtils.getTestDataPerRowAsObject(i));
					}
				} else {
					if (excelUtils.getCellData(i, 1).equals(testMethod)) {

						System.out.println("Loading: ExcelWorkSheet - Row # " + i);

						testDataSet.add(excelUtils.getTestDataPerRowAsObject(i));
					}
				}
			}
		} catch (Exception e) {
			System.out.println(e.getMessage() + e.getStackTrace());
		} finally {
			excelUtils.close();
		}
		this.setDataProviderSize(testDataSet.size());
		return testDataSet;
	}

	public List<Object[]> getTestDataFromExcelForSoapAPI(File testFile, String testMethod) throws Exception {
		// File file = getTestDataFile(testFile);
		testFileName = testFile;
		testMethodName = testMethod;

		ExcelUtils excelUtils = new ExcelUtils(testFile);

		testDataSet = new ArrayList<Object[]>();

		try {

			int lastRow = excelUtils.getLastRowNum();
			int temp = 0;
			for (int i = 0; i <= lastRow; i++) {

				if (runSmokeTests || priorityP0 || priorityP1) {

					if (runSmokeTests) {
						if (excelUtils.getCellData(i, 1).equals(testMethod)
								&& excelUtils.getCellData(i, 2).toUpperCase().equals("Y")) {
							System.out.println("Loading: ExcelWorkSheet - Row # " + i);
							testDataSet.add(excelUtils.getTestDataPerRowAsObject(i));
							temp = i;
						}
					}

					if (priorityP0) {
						if (excelUtils.getCellData(i, 1).equals(testMethod)
								&& excelUtils.getCellData(i, 3).toUpperCase().equals("Y")) {
							if (temp != i) {
								testDataSet.add(excelUtils.getTestDataPerRowAsObject(i));
								System.out.println("Loading: ExcelWorkSheet - Row # " + i);
								temp = i;
							}

						}
					}

					if (priorityP1) {
						if (excelUtils.getCellData(i, 1).equals(testMethod)
								&& excelUtils.getCellData(i, 4).toUpperCase().equals("Y")) {
							if (temp != i) {
								testDataSet.add(excelUtils.getTestDataPerRowAsObject(i));
								System.out.println("Loading: ExcelWorkSheet - Row # " + i);
							}
						}
					}
				} else {
					if (excelUtils.getCellData(i, 1).equals(testMethod)) {

						System.out.println("Loading: ExcelWorkSheet - Row # " + i);

						testDataSet.add(excelUtils.getTestDataPerRowAsObject(i));
					}
				}
			}
		} catch (Exception e) {
			System.out.println(e.getMessage() + e.getStackTrace());
		} finally {
			excelUtils.close();
		}
		this.setDataProviderSize(testDataSet.size());
		return testDataSet;
	}

	/* getRowDataFromExcel let you read just row data with out column header */

	public List<Object[]> getRowDataFromExcel(File testFile, String testMethod)
			throws IOException, EncryptedDocumentException, InvalidFormatException {
		// File file = getTestDataFile(testFile);

		ExcelUtils excelUtils = new ExcelUtils(testFile);

		testDataSet = new ArrayList<Object[]>();

		try {

			int lastRow = excelUtils.getLastRowNum();

			for (int i = 0; i <= lastRow; i++) {
				if (excelUtils.getCellData(i, 1).equals(testMethod)) {
					System.out.println("Loading: ExcelWorkSheet - Row # " + i);

					testDataSet.add(excelUtils.getTestDataInRow(i));
				}
			}
		} catch (Exception e) {
			System.out.println(e.getMessage() + e.getStackTrace());
		} finally {
			excelUtils.close();
		}
		this.setDataProviderSize(testDataSet.size());
		return testDataSet;
	}

	public String getResponseValueByXmlPath(RestResponse response, String xPath) throws FileNotFoundException,
			ParserConfigurationException, IOException, SAXException, XPathExpressionException, TransformerException {
		XmlUtils xmlUtils = new XmlUtils(response.getBodyAsString());
		return xmlUtils.getXPathValue(xPath);
	}

	public void assertResponseValueByXmlPath(RestResponse response, String xPath, String value)
			throws FileNotFoundException, ParserConfigurationException, IOException, SAXException,
			XPathExpressionException, TransformerException {
		Assert.assertEquals(this.getResponseValueByXmlPath(response, xPath), value);
	}

	@BeforeMethod(alwaysRun = true)
	public void beforeMethod(java.lang.reflect.Method testMethod)
			throws MalformedURLException, IOException, NoSuchAlgorithmException, KeyManagementException {
		System.out.println("BeforeMethod -- Test Method Name: " + testMethod.getName());
		Test test = testMethod.getAnnotation(Test.class);
		logger = ReportFactory.getLooger(testMethod.getName(),test.description());
		logger.info("Start Test");

	}

	@AfterMethod(alwaysRun = true)
	public void afterMethod(ITestResult result) throws IOException {

		System.out.println("--------- AfterMethod: --------------");
		String testPlanName = StringUtil.subStringRegex("[^.]*$", result.getInstanceName());

		if (logger.getStatus() == LogType.Error.getLogStatus())
			ReportFactory.printAndLog(LogType.Error, "Error(s) detected in the Test");
		else if (result.getStatus() == ITestResult.FAILURE)
			ReportFactory.printAndLog(LogType.Fail, result.getThrowable().getMessage());
		else if (result.getStatus() == ITestResult.SKIP)
			ReportFactory.printAndLog(LogType.Skip, "The test was skiped.");
		else
			ReportFactory.printAndLog(LogType.Pass, "Test passed");

		ReportFactory.getExtendTest().assignCategory(testName, testPlanName);
	    result.getTestContext().setAttribute("DATA_PROVIDER_SIZE", Integer.valueOf(this.getDataProviderSize()));
		WebUIDriver.cleanUp();
	}

	@SuppressWarnings("static-access")
	@BeforeClass(alwaysRun = true)
	@Parameters({ "environment", "webDriverGrid", "webRunMode", "browser", "responsePath", "dataProviderPath",
			"SmokeTests", "PriorityP0", "PriorityP1" })
	public void beforeClass(String environment, String webDriverGrid, String webRunMode, String browser,
			ITestContext testContext, String pResponsePath, String pDataProviderPath, String smokeTests,
			String priorityP0, String priorityP1) throws XPathExpressionException, FileNotFoundException,
			ParserConfigurationException, IOException, SAXException, TransformerException

	{
		new File("./output/TestCaseResults/").mkdirs();
		FileUtil.deleteFilesInDirectory(new File("./output/Downloads"));
		System.out.println("--------- Start: --------------");
		Env = Environment.valueOf(environment);
		System.out.println("Enviornment is " + Env + ", Grid is: " + webDriverGrid + " run Mode is: " + webRunMode);
		ContextManager.getContext().setEnvironment(Env);
		ContextManager.getContext().setWebRunBrowser(browser);
		ContextManager.getContext().setWebDriverGrid(webDriverGrid);
		ContextManager.getContext().setWebRunMode(webRunMode);
		createdByGid = ContextManager.getContext().getUserGid();
		
		
		testContext.setAttribute("DATA_PROVIDER_SIZE", Integer.valueOf(this.getDataProviderSize()));

		ReportFactory.getReporter().setSystemInfo("Environment", Env.toString());
		ReportFactory.getReporter().setSystemInfo("Grid", webDriverGrid);
		ReportFactory.getReporter().setSystemInfo("Run Mode", webRunMode);

		if (!smokeTests.isEmpty())
			this.runSmokeTests = Boolean.parseBoolean(smokeTests);
		if (!priorityP0.isEmpty())
			this.priorityP0 = Boolean.parseBoolean(priorityP0);
		if (!priorityP1.isEmpty())
			this.priorityP1 = Boolean.parseBoolean(priorityP1);

		testName = testContext.getName();
		responsePath = pResponsePath;
		dataProviderPath = pDataProviderPath;

		try {

			PoolManager.init();
		} catch (Exception ex) {
			System.err.println(ex.getMessage() + ex.getStackTrace());
		}
	}

	@AfterClass(alwaysRun = true)
	public static void afterClass() {

		//FileUtil.deleteFilesInDirectory(new File("./output/Downloads"));
		System.out.println("--------- End: --------------");
	}

	public void setTestPlanName(String testPlanName) {
		this.testPlanName = testPlanName;
		System.out.println("Set Test Plan : " + this.testPlanName);
	}

	public static Map<String, String> getParameters(Object[] testData) throws IOException {
		return ExcelUtils.getParameters(testData);
	}

	public static Object[] addParam(Object[] params, String key, String value) {
		List<Object> paramsList = new ArrayList<Object>(Arrays.asList(params));
		paramsList.add(key + ":" + value);
		return paramsList.toArray();
	}
}
