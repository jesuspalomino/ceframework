package com.complianceease.framework.common;

import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.MediaEntityBuilder;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;
import com.aventstack.extentreports.reporter.configuration.Theme;
import com.complianceease.framework.api.enums.LogType;
import com.complianceease.framework.common.configuration.ContextManager;

public class ReportFactory {
	
	public static ExtentReports reporter = getReporter();
	private static ExtentTest logger = null;
	public static String fileName;
	static Map<Long, ExtentTest> extentTestMap = new HashMap<Long, ExtentTest>();
	
	
	public static synchronized ExtentReports createInstance(String fileName)
	{
		
		ExtentHtmlReporter htmlReporter = new ExtentHtmlReporter(fileName);
	    htmlReporter.config().setChartVisibilityOnOpen(false);
	    htmlReporter.config().setTheme(Theme.STANDARD);
	    htmlReporter.config().setDocumentTitle(fileName);
	    htmlReporter.config().setEncoding("utf-8");
	    htmlReporter.config().setReportName(fileName);
	        
	    reporter = new ExtentReports();
	    reporter.attachReporter(htmlReporter);
	        
	    return reporter;
	}
	
	public static synchronized ExtentReports getReporter() {
		
		if (reporter == null) {
			fileName = setReportName("Report");
			reporter = createInstance("./output/TestCaseResults/" + fileName);
		}
		
		return reporter;			
	}
	
    public static synchronized ExtentTest getLooger(String testMethodName,String description) {	
    	
    	logger = reporter.createTest(testMethodName,"<b>Description: </b>" + description);
    	
    	extentTestMap.put(Thread.currentThread().getId(),logger);
    
		return extentTestMap.get(Thread.currentThread().getId());
	}
    
    public static synchronized ExtentTest getExtendTest() {
	 
		return extentTestMap.get(Thread.currentThread().getId());
	}
    
    public static void OpenReport()
    {
    	try {
    		Runtime r = Runtime.getRuntime();
    		File file = new File("./output/TestCaseResults/" + fileName );
    		String absolutePath = file.getAbsolutePath().replace(".\\", "");
			if(ContextManager.webBrowser.equals("iexplore"))
			{
				// IE driver
				r.exec("c:\\program files\\internet explorer\\iexplore.exe " + absolutePath);
			}
			else
			{
				// Chrome driver
				r.exec("C:\\Program Files (x86)\\Google\\Chrome\\Application\\chrome.exe \"" + absolutePath+"\"");
			}


    	} catch (Exception e) {
    		printAndLog(LogType.Error, e.getMessage());
    	}
    }

	private static String setReportName(String reportName)
	{
		DateFormat dateFormat = new SimpleDateFormat(" yyyy-MM-dd HH.mm.ss");
		Date date = new Date();
		
		return  String.format(reportName + dateFormat.format(date)+ ".html");
	}
	
	public static void printAndLog(LogType logStatus, String message)
	{
		logger = extentTestMap.get(Thread.currentThread().getId());

		if(logger != null)
		{
			switch(logStatus)
			{
			    case Error:
			    	logger.error(message);
			    break;
			    case Fail:
			    	logger.fail(message);
			    break;
			    case Info:
			    	logger.info(message);
			    break;
			    case Pass:
			    	logger.pass(message);
			    break;
			    case Skip:
			    	logger.skip(message);
			    case Warning:
			    	logger.warning(message);
			    break;
			default:
				break;
			 
			}
		}
		
		System.out.println(message);
	}
	
	public static void insertScreenShot(String path)
	{
		logger = extentTestMap.get(Thread.currentThread().getId());
		
		try {
			
			logger.info("",MediaEntityBuilder.createScreenCaptureFromPath("./screenshots/"+ path).build());
			
		} catch (IOException e) {
			
			printAndLog(LogType.Error,e.getMessage());
		}
	}
	
}
