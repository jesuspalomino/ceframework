package com.complianceease.framework.common.configuration;

public enum Property {
	WebserviceAuthKey,
	EndPoint,
	EnvironmentName,
	ChromeDriverPath,
	IExplorerDriverPath,
	SQLServerDriver,
	SQLServerUrl,
	SQLServerLogin,
	SQLServerPassword,
	UserGid,
	NotifyEmail,
	RCCertifyUrl,
	RCConnectUrl,
	AdminEndPoint,
	FannieMaeCertPassword
}
