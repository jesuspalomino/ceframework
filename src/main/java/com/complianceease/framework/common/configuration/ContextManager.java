package com.complianceease.framework.common.configuration;

import com.complianceease.framework.api.enums.Environment;
import com.complianceease.framework.api.util.XmlUtils;

import java.io.File;
import java.io.InputStream;
import java.util.Map;

/**
 * Created by knimashakavi on 12/23/2015.
 */
public class ContextManager {
	/*
      TBD - This should read values from properties file..
	 */
	private XmlUtils xmlUtils;
	private static final String environmentXPath = "/Properties/Environments/Environment[@id=\"%s\"]/%s/text()";

	public static  String webBrowser="";
	private static Environment currentEnvironment;
	private static String webDriverGrid;
	private static String webRunMode;
	public boolean isJavaScriptEnabled() {return true;}
	public boolean ignoreUnTrustedCertificates(){ return true;}
	private static final String PropertiesXmlFile = "../CEFramework/src/main/resources/properties.xml";

	public static ContextManager singleton = new ContextManager();
	private ContextManager() {
		try{
			//        	File properties = new File(PropertiesXmlFile);
			//        	xmlUtils = new XmlUtils(properties);
			InputStream is = Thread.currentThread().getContextClassLoader().getResourceAsStream("properties.xml");
			xmlUtils = new XmlUtils(is);
			System.out.println("Properties loaded...");	
		}catch (Exception e) {
			e.printStackTrace();
		}
	}
	public static ContextManager getContext() {
		return singleton;
	}

	public Map<Integer, Boolean> getTestPCMap() {
		return null;
	}
	public String getUrlConvertClass() {
		return "";
	}

	public String getWebBrowserVersion() {
		return null;
	}

	public String getWebDriverGrid() {
		return webDriverGrid;	
	}

	public String getWebRunBrowser() {
		return webBrowser;
	}
	
	public String setWebRunBrowser(String browser) {
		return webBrowser = browser;
	}

	public String getFirefoxBinPath() {
		return null;
	}

	public String getFirefoxUserProfilePath() {
		return null;
	}

	public String getIEDriverPath(){
		System.out.println("Current enviornment is "+currentEnvironment);
		String iEDriverPath = getConfigPropertyByXPath(currentEnvironment, Property.IExplorerDriverPath);
		System.out.println("IExploreDriver path is: "+iEDriverPath);
		return getConfigPropertyByXPath(currentEnvironment, Property.IExplorerDriverPath);
	}

	public int getImplicitWaitTimeout() {
		return 180;
	}

	public int getExplicitWaitTimeout() {
		return 180;
	}
	public String getTestNGDirectory() {return null;}
	public String getPlatform() {
		return null;
	}
	public String getWebRunMode() {
		return webRunMode;
	}

	public int getWebSessionTimeout() {
		return 60;
	}
	public static String getPCSettings() {
		return null;
	}
	public String getCurrentEnvironment() {
		return currentEnvironment.name();
	}
	public String getEnvironmentName() {
		String envName = getConfigPropertyByXPath(currentEnvironment, Property.EnvironmentName);
		System.out.println("Enviornment name .."+currentEnvironment+" name"+envName);

		return envName;
	}
	public String getEndPoint( ) {
		String endPoint = getConfigPropertyByXPath(currentEnvironment, Property.EndPoint);
		System.out.println("End point .."+currentEnvironment+" name"+endPoint);

		return endPoint;
	}
	public String getAdminEndPoint( ) {
		String endPoint = getConfigPropertyByXPath(currentEnvironment, Property.AdminEndPoint);
		System.out.println("Admin End point .."+currentEnvironment+" name"+endPoint);

		return endPoint;
	}
	public String getWebServiceAuthKey( ) {
		return getConfigPropertyByXPath(currentEnvironment,Property.WebserviceAuthKey);
	}
	public String getChromeDriverPath() {
		System.out.println("Current enviornment is "+currentEnvironment);
		String chromeDriverPath = getConfigPropertyByXPath(currentEnvironment, Property.ChromeDriverPath);
		System.out.println("Chromedriver path is: "+chromeDriverPath);
		return getConfigPropertyByXPath(currentEnvironment, Property.ChromeDriverPath);

	}
	public String getFannieMaeCertPassword( ) {
		String fannieMaeCertPassword = getConfigPropertyByXPath(currentEnvironment, Property.FannieMaeCertPassword); 
		System.out.println("Fannie Mae Cert Password .."+ currentEnvironment + " name"+ fannieMaeCertPassword);

		return fannieMaeCertPassword;
	}

	public String getSQLServerDriver() {
		return getConfigPropertyByXPath(currentEnvironment, Property.SQLServerDriver);
	}
	public String getSQLServerUrl() {
		return getConfigPropertyByXPath(currentEnvironment, Property.SQLServerUrl);
	}
	public String getSQLServerLogin() {
		return getConfigPropertyByXPath(currentEnvironment, Property.SQLServerLogin);
	}
	public String getSQLServerPassword() {
		return getConfigPropertyByXPath(currentEnvironment, Property.SQLServerPassword);
	}
	public String getConfigPropertyByXPath(Environment environment, Property key) {
		String value = "";
		try {
			value = xmlUtils.getXPathValue(String.format(environmentXPath, environment.name(), key.name()));
		} catch (Exception e){e.printStackTrace();}
		return value;
	}
	public static void setEnvironment(Environment env) {
		currentEnvironment = env;
	}

	public String getUserGid()
	{
		return getConfigPropertyByXPath(currentEnvironment,Property.UserGid);
	}

	public static void setWebDriverGrid(String gridName) {
		webDriverGrid = gridName;
	}

	public static void setWebRunMode(String runmode) {
		webRunMode = runmode;
	}	
	public String getRCCertifyUrl(){
		String rCCertifyUrl = getConfigPropertyByXPath(currentEnvironment, Property.RCCertifyUrl);
		System.out.println("RCCertifyUrl: " + rCCertifyUrl);
		return rCCertifyUrl;
	}
	public String getRCConnectUrl(){
		String rCConnectUrl = getConfigPropertyByXPath(currentEnvironment, Property.RCConnectUrl);
		System.out.println("RCConnectUrl: " + rCConnectUrl);
		return rCConnectUrl;
	}
	public String getNotifyEmail(){
		String notifyEmail = getConfigPropertyByXPath(currentEnvironment, Property.NotifyEmail);
		System.out.println("RCCertifyUrl: " + notifyEmail);
		return notifyEmail;
	}
}


