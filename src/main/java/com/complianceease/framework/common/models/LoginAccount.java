package com.complianceease.framework.common.models;

public class LoginAccount {

	private String UserName;
	private String Password;
	private Integer CredentialType;
	private String ClientCode;
	private String Environment;
	
	public String getUserName() {
		return UserName;
	}
	public void setUserName(String userName) {
		UserName = userName;
	}
	public String getPassword() {
		return Password;
	}
	public void setPassword(String password) {
		Password = password;
	}
	public Integer getCredentialType() {
		return CredentialType;
	}
	public String getClientCode() {
		return ClientCode;
	}
	public void setCredentialType(Integer credentialType) {
		CredentialType = credentialType;
	}
	public void setClientCode(String clientCode) {
		ClientCode = clientCode;
	}
	public String getEnvironment() {
		return Environment;
	}
	public void setEnvironment(String environment) {
		Environment = environment;
	}

}
