package com.complianceease.framework.common;

import java.util.List;
import java.util.Map;

import org.testng.IReporter;
import org.testng.IResultMap;
import org.testng.ISuite;
import org.testng.ISuiteResult;
import org.testng.ITestContext;
import org.testng.ITestResult;
import org.testng.xml.XmlSuite;

import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;


public class ExtentReporterNG implements IReporter  {

	@Override
    public void generateReport(List<XmlSuite> xmlSuites, List<ISuite> suites, String outputDirectory) {
    	
        for (ISuite suite : suites) {
            Map<String, ISuiteResult> result = suite.getResults();

            for (ISuiteResult r : result.values()) {
                ITestContext context = r.getTestContext();
                buildTestNodes(context, Status.SKIP);
            }
        }
      
        ReportFactory.reporter.flush();
        ReportFactory.OpenReport();
    }
    
    private void buildTestNodes(ITestContext context, Status status) {
    	ExtentTest test;
    	IResultMap resultMap =  context.getSkippedTests();
    	
        if (resultMap.size() > 0) {
            for (ITestResult result : resultMap.getAllResults()) {
            	
            	test = ReportFactory.reporter.createTest(result.getMethod().getMethodName());
 
                 if (result.getThrowable() != null)
                	  test.log(status, result.getThrowable().getMessage());
            
                test.assignCategory(context.getName(), result.getInstanceName().substring(32));
            }
        }
    }    
}
