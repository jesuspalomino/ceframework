package com.complianceease.framework.web.element;

import org.openqa.selenium.By;

public class Link extends HTMLBaseElement {

	public Link(String label, By by) {
		super(label, by);
	}

  	public boolean isLive() {
		try {
 			openLink( getAttribute("href") );
			return true;
		} catch (Exception e) {
		}
		return false;
	}

	protected String openLink( String url) throws Exception {
 //		return URLHelper.open( url );
		return url;
	}

}