package com.complianceease.framework.web.element;

import org.openqa.selenium.By;

public class Label extends HTMLBaseElement {
	public Label(String label, By by) {
		super(label, by);
	}

 	public boolean patternExists(String pattern){
		String text = getText();
		return (text != null && (text.contains(pattern) ||text.matches(pattern)));
	}

}