package com.complianceease.framework.web.element;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriverException;

public class TextField extends HTMLBaseElement {
	public TextField(String label, By by) {
		super(label, by);
	}

	public void clear() {
 		findElement();
		webElement.clear();
	}

	public void typeKeys(String keysToSend) {
 		findElement();
 		webElement.sendKeys(keysToSend);
	}

	public void typeKeys(Keys keysToSend) {
		findElement();
		webElement.sendKeys(keysToSend);
	}

	public void type(String keysToSend) {
		if (keysToSend == null) {
			return;
		}

		waitForElementToPresent(this);
		try {
			clear();
		} catch (WebDriverException e) {
			e.printStackTrace();
		}
		typeKeys(keysToSend);
	}
}
