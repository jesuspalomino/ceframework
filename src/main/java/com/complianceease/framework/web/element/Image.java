package com.complianceease.framework.web.element;

import org.openqa.selenium.By;
import org.openqa.selenium.interactions.Actions;

public class Image extends HTMLBaseElement {

	public Image(String label, By by) {
		super(label, by);
	}
	public int getHeight() {
		return super.getSize().getHeight();
	}
	public int getWidth() {
		return super.getSize().getWidth();
	}

	public void click(int x, int y) {
		new Actions(webDriver).moveToElement(webDriver.findElement(by), x, y).click().build().perform();
	}
}