package com.complianceease.framework.web.element;

import org.openqa.selenium.By;

public class CheckBox extends HTMLBaseElement {

	public CheckBox(String label, By by) {
		super(label, by);
	}

	public void check() {
 		if (!isSelected()) {
			super.click();
		}
	}

  	public boolean isSelected() {
		findElement();
		return webElement.isSelected();
	}

	public void uncheck() {
		if (isSelected()) {
			super.click();
		}
	}
}