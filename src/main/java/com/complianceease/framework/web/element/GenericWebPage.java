package com.complianceease.framework.web.element;

import com.complianceease.framework.api.util.ScreenShotUtil;
import com.complianceease.framework.common.ReportFactory;
import com.complianceease.framework.web.util.ThreadHelper;
import com.complianceease.framework.web.util.WebUIDriver;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.NoAlertPresentException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import java.io.IOException;

/**
 * Base Page Object Model for all pages in the application.
 *
 * @author knimashakvi
 */
public class GenericWebPage<T extends GenericWebPage<T>> implements IWebPage {
	private static final Logger logger = Logger.getLogger(GenericWebPage.class);
	private static final long EXPLICIT_WAIT_TIME_OUT = WebUIDriver.getWebUIDriver().getExplicitWait();
	private WebDriver driver;
	protected final WebUIDriver webUIDriver = WebUIDriver.getWebUIDriver();
	private String title = null;
	private String url = null;
	private String location = null;
	private String bodyText = null;
	private String htmlSource = null;
	private String htmlSavedToPath = null;
	private String pageId = null;
	public final String IMAGENAMECHECKED = "[ X ]";

	public GenericWebPage() {
		this.driver = WebUIDriver.getWebDriver();
	}

	public void assertElementNotPresent(HTMLBaseElement element) {
 		assertHTML(!element.isElementPresent(), element.toString() + " found.");
	}

	public void assertElementPresent(HTMLBaseElement element) {
 		assertHTML(element.isElementPresent(), element.toString() + " not found.");
	}

	protected void assertHTML(boolean condition, String message) {
		if (!condition) {
			Assert.assertTrue(condition, message);
		}
	}

	public void assertTextNotPresent(String text) {
 		assertHTML(!getBodyText().contains(text), "Text= {" + text + "} found.");
	}

	public void assertTextPresent(String text) {
 		assertHTML(getBodyText().contains(text), "Text= {" + text + "} not found.");
	}

	/**
	 * Ensure that the page is currently loaded.
	 *
	 * @return The page.
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	public T get() throws Exception {
		if (url != null || (getPageUrl() != null && !"".equals(getPageUrl()))) {
			load();
		}
		isLoaded();
		return (T) this;
	}

	@Override
    public String getBodyText() {
	    if (bodyText == null) {
	        bodyText = driver.findElement(By.tagName("body")).getText();
	    }
	    return bodyText;
	}

	public WebDriver getDriver() {
		return driver;
	}

	public int getElementCount(HTMLBaseElement element) {
		return element.getAllElements().size();
	}

	public String getHtmlSavedToPath() {
		return htmlSavedToPath;
	}

	@Override
    public String getHtmlSource() {
		return htmlSource;
	}

 	@Override
    public String getLocation() {
		return location;
	}

	public String getPageId() {
		return pageId;
	}

 	@Override
    public String getTitle() {
		return driver.getTitle();
	}

	public String getUrl() {
		return url;
	}

	public String getPageUrl() {
		return null;
	}

	protected void isLoaded() throws Exception {
		if (!isPageLoaded()) {
 			throw new Exception("Page not loaded");
		}
	}

	/**
	 * Determine whether the page is loaded or not. It return true by default. Override is needed in the real page if we want to add any condition for page
	 * load. The expected sample usage is:
	 *
	 * <pre class="code">
	 * return txtUserID.isElementPresent();
	 * return txtUserID.isDisplayed();
	 * </pre>
	 *
	 */
	protected boolean isPageLoaded() {
		return true;
	}

	/**
	 * Load page URL.
	 *
	 */
	protected void load() {
 		driver.get(getPageUrl() == null || "".equals(getPageUrl()) ? url : getPageUrl());
		driver.switchTo().defaultContent();
		url = null;
	}

 	@SuppressWarnings("unchecked")
	public T refresh() throws Exception {
 		driver.navigate().refresh();
		isLoaded();
		return (T) this;
	}

	public void selectFrame(By by) {
 		driver.switchTo().frame(driver.findElement(by));
	}

	public void setBodyText(String bodyText) {
		this.bodyText = bodyText;
	}

	public void setDriver(WebDriver driver) {
		this.driver = driver;
	}

	public void setHtmlSavedToPath(String htmlSavedToPath) {
		this.htmlSavedToPath = htmlSavedToPath;
	}

	public void setHtmlSource(String htmlSource) {
		this.htmlSource = htmlSource;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public void setPageId(String pageId) {
		this.pageId = pageId;
	}
	
	public boolean containsText(String text)
	{
		return this.driver.getPageSource().contains(text);
	}
	

	public void setTitle(String title) {
		this.title = title;
	}

	@SuppressWarnings("unchecked")
	public T setUrl(String url) {
		this.url = url;
		return (T) this;
	}

	public void waitForElementToBeClickable(HTMLBaseElement element) {
		Assert.assertNotNull(element, "Element can't be null");
 		WebDriverWait wait = new WebDriverWait(driver, EXPLICIT_WAIT_TIME_OUT);
		wait.until(ExpectedConditions.elementToBeClickable(element.getBy()));
	}

	public void waitForElementToBeSelected(HTMLBaseElement element) {
		Assert.assertNotNull(element, "Element can't be null");
 		WebDriverWait wait = new WebDriverWait(driver, EXPLICIT_WAIT_TIME_OUT);
		wait.until(ExpectedConditions.elementToBeSelected(element.getBy()));
	}

	public void waitForElementToBeVisible(HTMLBaseElement element) {
		Assert.assertNotNull(element, "Element can't be null");
 		WebDriverWait wait = new WebDriverWait(driver, EXPLICIT_WAIT_TIME_OUT);
		wait.until(ExpectedConditions.visibilityOfElementLocated(element.getBy()));
	}
	public void waitForElementToBeVisible(HTMLBaseElement element,int seconds) {
		Assert.assertNotNull(element, "Element can't be null");
		WebDriverWait wait = new WebDriverWait(driver, seconds);
		wait.until(ExpectedConditions.visibilityOfElementLocated(element.getBy()));
	}

	public void waitForElementToDisappear(HTMLBaseElement element) {
		Assert.assertNotNull(element, "Element can't be null");
 		WebDriverWait wait = new WebDriverWait(driver, EXPLICIT_WAIT_TIME_OUT);
		wait.until(ExpectedConditions.invisibilityOfElementLocated(element.getBy()));
	}

	public void waitForElementToPresent(HTMLBaseElement element) {
		Assert.assertNotNull(element, "Element can't be null");
 		WebDriverWait wait = new WebDriverWait(driver, EXPLICIT_WAIT_TIME_OUT);
		wait.until(ExpectedConditions.presenceOfElementLocated(element.getBy()));
	}

	public void waitForSeconds(int seconds) {
		ThreadHelper.waitForSeconds(seconds);
	}

	public void waitForTextToPresent(HTMLBaseElement element, String text) {
		Assert.assertNotNull(text, "Text can't be null");
 		WebDriverWait wait = new WebDriverWait(driver, EXPLICIT_WAIT_TIME_OUT);
		wait.until(ExpectedConditions.textToBePresentInElement(element.getBy(), text));
	}
	
	public void SingOut()
	{
		this.driver.findElement(By.xpath("//a[contains(text(),'Sign Out')]")).click();
	}
	
	public void capturePage()
    {
    	ScreenShotUtil.captureScreenshot(this.driver);
    }
	
	public void goBack(int number)
	{
		for(int i = 0; i< number;i++)
		{
			this.driver.navigate().back();
		}
	}
	
	public void switchToLastWindow()
    {
          for(String currentWindow: driver.getWindowHandles())        
          {
                driver.switchTo().window(currentWindow);    
          }
    }

    public boolean isAlertPresent() {
	        try {
	            driver.switchTo().alert();
	            return true;
	        } catch (NoAlertPresentException e) {
	            return false;
	        }
	}
     
    public String getCurrentUrl() {
     return this.driver.getCurrentUrl();
    }
}
