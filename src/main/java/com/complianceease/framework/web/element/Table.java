package com.complianceease.framework.web.element;

import org.openqa.selenium.By;
import org.openqa.selenium.NotFoundException;
import org.openqa.selenium.WebElement;

import java.util.List;

public class Table extends HTMLBaseElement {
	private List<WebElement> rows = null;
	private List<WebElement> columns = null;

	private boolean hasBody = false;

	public Table(String label, By by) {
		super(label, by);
	}


	public void findElement() {
		super.findElement();
		try {
			rows = webElement.findElements(By.tagName("tr"));
		} catch (NotFoundException e) {
		}
	}

	public int getColumnCount() {
		if (rows == null)
			findElement();
		
		// Need to check whether rows is null AND whether or not the list of rows is empty
		if (rows != null && !rows.isEmpty()) {
			try {
				columns = rows.get(0).findElements(By.tagName("th"));
			} catch (NotFoundException e) {
			}
			if (columns == null || columns.size() == 0) {
				try {
					columns = rows.get(0).findElements(By.tagName("td"));
				} catch (NotFoundException e) {
				}
			}
		}
		if (columns != null)
			return columns.size();
		return 0;
	}

	public List<WebElement> getColumns() {
		return columns;
	}

	/**
	 * Get table cell content
	 * 
	 * @param row
	 * @param column
	 */
	public String getContent(int row, int column) {
		if (rows == null)
			findElement();

		if (rows != null && !rows.isEmpty()) {
			try {
				columns = rows.get(row - 1).findElements(By.tagName("td"));
			} catch (NotFoundException e) {
			}
			if (columns == null || columns.size() == 0) {
				try {
					columns = rows.get(row - 1).findElements(By.tagName("th"));
				} catch (NotFoundException e) {
				}
			}
			return columns.get(column - 1).getText();
		}
		
		return null;
	}

 	public List<WebElement> getRows() {
		return rows;
	}

	public boolean isHasBody() {
		return hasBody;
	}

	public void setColumns(List<WebElement> columns) {
		this.columns = columns;
	}

	public void setHasBody(boolean hasBody) {
		this.hasBody = hasBody;
	}

	public void setRows(List<WebElement> rows) {
		this.rows = rows;
	}
}