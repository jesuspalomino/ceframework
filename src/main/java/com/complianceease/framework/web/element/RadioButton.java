package com.complianceease.framework.web.element;

import org.openqa.selenium.By;

public class RadioButton extends HTMLBaseElement {

	public RadioButton(String label, By by) {
		super(label, by);
	}

 	public void check() {
 		super.click();
	}

	@Override
	public void click() {
 		super.click();
	}

 	public boolean isSelected() {
		findElement();
		return webElement.isSelected();
	}
}