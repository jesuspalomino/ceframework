package com.complianceease.framework.web.element;

import org.openqa.selenium.By;

/**
 * Created by knimashakavi on 12/23/2015.
 */
public class Button extends HTMLBaseElement {
    public Button(String label, By by) {
        super(label, by);
    }
    public void submit() {
        findElement();
        webElement.submit();
    }
    public void sentKeys(String path) {
        findElement();
        webElement.sendKeys(path);
    }
}
