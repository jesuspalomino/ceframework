package com.complianceease.framework.web.element;

import com.complianceease.framework.api.util.ScreenShotUtil;
import com.complianceease.framework.web.util.WebUIDriver;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.openqa.selenium.*;
import org.apache.log4j.Logger;
import java.util.List;
import java.util.concurrent.TimeUnit;
public class HTMLBaseElement {

    protected static final Logger logger = Logger.getLogger(HTMLBaseElement.class);
    protected static final long EXPLICIT_WAIT_TIME_OUT = WebUIDriver.getWebUIDriver().getExplicitWait();

    protected WebDriver webDriver;
    protected WebUIDriver webUIDriver = WebUIDriver.getWebUIDriver();
    protected By by;
    protected WebElement webElement;
    protected String label;

    public HTMLBaseElement(String label, By by) {
        this.webDriver = webUIDriver.getDriver();
        this.label = label;
        this.by = by;
    }


    public WebDriver getWebDriver() {
        return webDriver;
    }

    public void setWebDriver(WebDriver webDriver) {
        this.webDriver = webDriver;
    }

    public By getBy() {
        return by;
    }

    public void setBy(By by) {
        this.by = by;
    }
    protected WebDriver refreshDriver() {
        return WebUIDriver.getWebDriver();
    }
    protected void findElement() {
    	webElement = webDriver.findElement(by);
    }
    
    public void init() {
        this.webDriver = refreshDriver();
        webElement = this.webDriver.findElement(by);
    }
    public void click() {
    	waitForElementToPresent(this);
        findElement();
        webElement.click();
    }
    public boolean isElementPresent() {
        int count = 0;
        try {
        	  WebUIDriver.getWebDriver().manage().timeouts().implicitlyWait(0, TimeUnit.SECONDS);
              count = WebUIDriver.getWebDriver().findElements(by).size();
              WebUIDriver.getWebDriver().manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        } catch (Exception e)  {
        }
        if (count == 0)
            return false;
        return true;
    }
    public boolean isElementExists() {
        int count = 0;
        try {
        	
             count = WebUIDriver.getWebDriver().findElements(by).size();
        } catch (Exception e)  {
        }
        if (count == 0)
            return false;
        return true;
    }
    public boolean isElementVisible() {
       boolean count=false;
       try {
             count = WebUIDriver.getWebDriver().findElement(by).isDisplayed();
       }catch (Exception e)  {
       }
        if (!count)
        {
            return false;
        }
        return true;
    }
    public String getAttribute(String name) {
        findElement();
        return webElement.getAttribute(name);
    }

    public Point getLocation() {
        findElement();
        return webElement.getLocation();
    }

    public String getTagName() {
        findElement();
        return webElement.getTagName();
    }
    public String getText() {
        findElement();
        return webElement.getText();
    }
    public void refresh()
    {
    	this.webDriver.navigate().refresh();
    }
    public Dimension getSize() {
        findElement();
        return webElement.getSize();
    }
    public int getHeight() {
        findElement();
        return webElement.getSize().getHeight();
    }

    public WebElement getElement() {
        findElement();
        return webElement;
    }

    public List<WebElement> getAllElements() {
        findElement();
        return getWebDriver().findElements(by);
    }
    
    public void capturePage()
    {
    	ScreenShotUtil.captureScreenshot(this.webDriver);
    }

	protected void waitForElementToPresent(final HTMLBaseElement element) {
		Assert.assertNotNull(element, "Attempting to wait on an empty HTMLBaseElement.");
 		try{
 		
			String type = element.getAttribute("Type") == null ? "" : element.getAttribute("Type") ;
			String tagName =  element.getTagName();
			
			
			if(type.equals("submit") ||tagName.equals("img") || tagName.equals("a"))
				ScreenShotUtil.captureScreenshot(this.webDriver);
			
	 		new WebDriverWait(webDriver, EXPLICIT_WAIT_TIME_OUT).until(
	 			new ExpectedCondition<Boolean>() {
	 				public Boolean apply(WebDriver driver) {
	 					return ((JavascriptExecutor) driver).executeScript("return document.readyState").equals("complete") && element.isElementPresent();
	 				}
	 			});
	 		
		 } catch (Exception  e) {
				ScreenShotUtil.captureScreenshot(this.webDriver);		
		 }
	}
	
	public void scrollUntilElement() throws InterruptedException
    {
          WebElement element = this.webDriver.findElement(by);
          ((JavascriptExecutor) this.webDriver).executeScript("arguments[0].scrollIntoView(true);", element);
    }
	
	public boolean isElementEnabled() {
        boolean count=false;
        try {
              count = WebUIDriver.getWebDriver().findElement(by).isEnabled();
        }catch (Exception e)  {
        }
     
        return count;
     }

 
}
