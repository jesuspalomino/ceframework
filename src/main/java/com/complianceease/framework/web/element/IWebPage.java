package com.complianceease.framework.web.element;


public interface IWebPage {
	String getBodyText();

	String getHtmlSource();

	String getLocation();

    String getTitle();
}
