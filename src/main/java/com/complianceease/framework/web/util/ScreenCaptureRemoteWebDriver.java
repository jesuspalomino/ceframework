package com.complianceease.framework.web.util;


import java.net.URL;

import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.DriverCommand;
import org.openqa.selenium.remote.RemoteWebDriver;

/* *
 * Created by knimashakavi 4/26/2016
 * */
public class ScreenCaptureRemoteWebDriver extends RemoteWebDriver implements TakesScreenshot {
	
	public ScreenCaptureRemoteWebDriver(URL url, DesiredCapabilities capabilities) {
		super(url, capabilities);
	}

	@Override
	public <X> X getScreenshotAs(OutputType<X> target)
			throws WebDriverException {
		if ((Boolean) getCapabilities().getCapability(
				CapabilityType.TAKES_SCREENSHOT)) {
			String output = execute(DriverCommand.SCREENSHOT).getValue()
					.toString();
			return target.convertFromBase64Png(output);
		}
		return null;
	}
}




