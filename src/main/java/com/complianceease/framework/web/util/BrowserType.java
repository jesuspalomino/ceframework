package com.complianceease.framework.web.util;

public enum BrowserType {
    FireFox("firefox"),
    InternetExplore("iexplore"),
    Chrome("chrome"),
    HtmlUnit("htmlunit"),
    Safari("safari");

    private String type;

    private BrowserType(String type) {
        this.type = type;
    }

    public String getType(){
        return this.type;
    }

}