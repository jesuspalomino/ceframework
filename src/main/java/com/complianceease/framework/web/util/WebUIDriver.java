package com.complianceease.framework.web.util;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.Platform;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.htmlunit.HtmlUnitDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.LocalFileDetector;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.safari.SafariDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import com.complianceease.framework.api.enums.LogType;
import com.complianceease.framework.common.ReportFactory;
import com.complianceease.framework.common.configuration.ContextManager;

public class WebUIDriver {

	private final String downloadDir = "./output/Downloads/";
	private final String VIDEO_URL = "Video URL : http://s3-us-west-2.amazonaws.com/6ca5b6bd-816f-45ea-b0af-d33a1627f21e/28b9c81a-542e-1eec-ee07-8e3def549720/play.html?%s";
	
    private static ThreadLocal<WebDriver> driverThreadLocal = new ThreadLocal<WebDriver>();
    private static ThreadLocal<WebUIDriver> webUIDriverThreadLocal = new ThreadLocal<WebUIDriver>();
    private boolean setAssumeUntrustedCertificateIssuer = true;
    private boolean setAcceptUntrustedCertificates = true;
    private boolean enableJavascript = true;

    private WebDriver driver;
    private String browser;
    private String mode;
    private String hubUrl;
    private String ffProfilePath;
    private String ffBinPath;
    private String ieDriverPath;
    private String chromeDriverPath;
    private int WebSessionTimeout;
    private int implicitWaitTimeout;
    private int explicitWaitTimeout;
    private String outputDirectory;
    private String browserVersion;
    private String platform;


    public static void cleanUp() {
        WebDriver driver = getWebDriver(false);
        if (driver != null) {
            try {
                 if (getWebUIDriver().browser != null && getWebUIDriver().browser.equalsIgnoreCase("iexplore")) {
                    try {
                        driver.manage().deleteAllCookies();
                    } catch (Exception e) {
                        // do nothing
                    }
                }
                driver.quit();
            } catch (WebDriverException ex) {

            }
        }
        driverThreadLocal.remove();
    }
    public static WebDriver getWebDriver(){
        return getWebDriver(true);
    }
    public static WebDriver getWebDriver(boolean isCreate){
        if(driverThreadLocal.get()==null && isCreate)
        {
            try {
                getWebUIDriver().createWebDriver();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return driverThreadLocal.get();
    }

    public static WebUIDriver getWebUIDriver() {
        if(webUIDriverThreadLocal.get()==null)
        {
            webUIDriverThreadLocal.set(new WebUIDriver());
        }
        return webUIDriverThreadLocal.get();
    }

    public static void setWebDriver(WebDriver driver) {
        if(getWebUIDriver() ==null)
            new WebUIDriver();
        driverThreadLocal.set(driver);
    }


    public WebUIDriver() {
        init();
        webUIDriverThreadLocal.set(this);
    }

    public WebUIDriver(String browser, String mode) {
        this.browser = browser;
        this.mode = mode;
        webUIDriverThreadLocal.set(this);
    }

    @SuppressWarnings("deprecation")
    public WebDriver createRemoteWebDriver(String browser, String mode) throws URISyntaxException {
        WebDriver driver = null;
        DesiredCapabilities capability = null;
        if (browser.equalsIgnoreCase(BrowserType.FireFox.getType())) {
            capability = new DesiredCapabilities();
            capability.setBrowserName(DesiredCapabilities.firefox().getBrowserName());
            String path = this.getFfProfilePath();
            FirefoxProfile profile = null;
            if (path != null) {
                ReportFactory.printAndLog(LogType.Info, "FireFoxProfile:" + this.getFfProfilePath());
                profile = new FirefoxProfile(new File(path));
            } else {
                ReportFactory.printAndLog(LogType.Info, "FireFoxProfile:null");
                profile = new FirefoxProfile();
            }
            if(setAcceptUntrustedCertificates==false)
            {
                profile.setAcceptUntrustedCertificates(false);
            }else
                profile.setAcceptUntrustedCertificates(true);

            if (setAssumeUntrustedCertificateIssuer == false)
                profile.setAssumeUntrustedCertificateIssuer(false);
            else
                profile.setAssumeUntrustedCertificateIssuer(true);

            profile.setPreference("capability.policy.default.Window.QueryInterface", "allAccess");
            profile.setPreference("capability.policy.default.Window.frameElement.get", "allAccess");
            profile.setPreference("capability.policy.default.HTMLDocument.compatMode.get", "allAccess");
            profile.setPreference("capability.policy.default.Document.compatMode.get", "allAccess");
            profile.setEnableNativeEvents(false);
            if(!enableJavascript)
                profile.setPreference("javascript.enabled", false);
            capability.setCapability(FirefoxDriver.PROFILE, profile);
        } else if (browser.equalsIgnoreCase(BrowserType.InternetExplore.getType())) {
            capability = DesiredCapabilities.internetExplorer();
            capability.setBrowserName(DesiredCapabilities.internetExplorer().getBrowserName());

        } else if (browser.equalsIgnoreCase(BrowserType.Chrome.getType())) {
            capability = DesiredCapabilities.chrome();
            capability.setBrowserName(DesiredCapabilities.chrome().getBrowserName());
        } else if (browser.equalsIgnoreCase(BrowserType.HtmlUnit.getType())) {
            capability = DesiredCapabilities.htmlUnit();
            capability.setBrowserName(DesiredCapabilities.htmlUnit().getBrowserName());
        } else if (browser.equalsIgnoreCase(BrowserType.Safari.getType())) {
            capability = DesiredCapabilities.safari();
            capability.setBrowserName(DesiredCapabilities.safari().getBrowserName());
        }

         if(enableJavascript)
            capability.setJavascriptEnabled(true);
        else
            capability.setJavascriptEnabled(false);
        capability.setCapability(CapabilityType.TAKES_SCREENSHOT, true);
        capability.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);
        capability.setCapability(CapabilityType.SUPPORTS_JAVASCRIPT, true);

        if (browserVersion != null) {
            capability.setVersion(browserVersion);
        }

        if (platform != null) {
            capability.setPlatform(Platform.valueOf(platform));
        }

        try {
        	if (mode.equalsIgnoreCase("ExistingGrid")) {
        		try {
        			if ( capability != null) {
        				System.out.println("enable videos...");
        			capability.setCapability("video", "True");
        			}
        			driver = new ScreenCaptureRemoteWebDriver(new URL(hubUrl), capability);
        			((RemoteWebDriver) driver).setFileDetector(new LocalFileDetector());
	        		} catch (WebDriverException ex) {
	        		// retry
	        		System.out.println("error in creating remote web driver, try again."+hubUrl);
	        		driver = new ScreenCaptureRemoteWebDriver(new URL(hubUrl), capability);
	        		((RemoteWebDriver) driver).setFileDetector(new LocalFileDetector());
	        		}
        			ReportFactory.printAndLog(LogType.Info, String.format(VIDEO_URL, ((RemoteWebDriver) driver).getSessionId()));
        		}
        	else if (browser.equalsIgnoreCase("firefox")) {
                if (this.ffBinPath != null)
                    System.setProperty("webdriver.firefox.bin", ffBinPath);
                    synchronized (this.getClass()) {
                        ReportFactory.printAndLog(LogType.Info, "start create firefox");
                        driver = new FirefoxDriver(capability);
                        ReportFactory.printAndLog(LogType.Info, "end create firefox"); 
                    }
                } else if (browser.equalsIgnoreCase("iexplore")) {
                    if (this.ieDriverPath != null) {
                    	 File ieDriverFile = new File(this.getClass().getResource("/IEDriverServer.exe").getPath());
                    	 ieDriverPath = ieDriverFile.getAbsolutePath();
                         System.setProperty("webdriver.ie.driver", ieDriverPath);
                    } else {
                        String osName = System.getProperty("os.name");
                        Enumeration<URL> urls = this.getClass().getClassLoader().getResources("IEDriverServer" + (osName.startsWith("Windows") ? ".exe" : ""));
                        while (urls.hasMoreElements()) {
                            URL url = urls.nextElement();
                            if (url != null && !url.getFile().contains("!")) {
                                System.setProperty("webdriver.ie.driver", url.getFile());
                                break;
                            }
                        }
                    }
                    capability = new DesiredCapabilities();
                    capability.setCapability(InternetExplorerDriver.INTRODUCE_FLAKINESS_BY_IGNORING_SECURITY_DOMAINS,true);
                    capability.setCapability("requireWindowFocus", false);
                    capability.setCapability("ie.ensureCleanSession", true);
                    
                    //adding capabilities as click operation is not working in IE 
                    capability.setCapability("nativeEvents", false);
                    capability.setCapability("unexpectedAlertBehaviour", "accept");
                    capability.setCapability("ignoreProtectedModeSettings", true);
                    capability.setCapability("disable-popup-blocking", true);
                    capability.setCapability("enablePersistentHover", true);
                    capability.setCapability("ignoreZoomSetting", true);                        

                driver = new InternetExplorerDriver(capability);
                } else if (browser.equalsIgnoreCase("chrome")) {
                    HashMap<String, Object> chromePrefs = new HashMap<String, Object>();
                    chromePrefs.put("safebrowsing.enabled", "true");
     		        chromePrefs.put("profile.default_content_settings.popups", 0);
     		        chromePrefs.put("download.default_directory", new File(downloadDir).getAbsolutePath().replace(".\\", "").toString());
                	ChromeOptions options = new ChromeOptions();
                	HashMap<String, Object> chromeOptionsMap = new HashMap<String, Object>();
                	options.setExperimentalOption("prefs", chromePrefs);
     		        options.addArguments("--test-type");
     		        options.addArguments("--disable-extensions");
     		        DesiredCapabilities cap = DesiredCapabilities.chrome();
    		        cap.setCapability(ChromeOptions.CAPABILITY, chromeOptionsMap);
    		        cap.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);
    		        cap.setCapability(ChromeOptions.CAPABILITY, options);
    		        
    		       File file = new File(this.chromeDriverPath) ;
                   if (this.chromeDriverPath != null && file.exists()) {
                	   System.setProperty("webdriver.chrome.driver", this.chromeDriverPath);
                    } else {
                        String osName = System.getProperty("os.name");
                        Enumeration<URL> urls = this.getClass().getClassLoader().getResources("chromedriver" + (osName.startsWith("Windows") ? ".exe" : ""));
                        while (urls.hasMoreElements()) {
                            URL url = urls.nextElement();
                            if (url != null && !url.getFile().contains("!")) {
                                System.setProperty("webdriver.chrome.driver", url.getFile());
                                break;
                            }
                        }

                    }
                    driver = new ChromeDriver(options);

                } else if (browser.equalsIgnoreCase("htmlunit")) {
                    driver = new HtmlUnitDriver(capability);
                } else if (browser.equalsIgnoreCase("safari")) {
                     driver = new SafariDriver();
                }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return driver;
    }

    public WebDriver createWebDriver() throws Exception {
        driver = createRemoteWebDriver(this.browser, this.mode);

        //Implicit Waits to handle dynamic element. The default value is 5 seconds.
        driver.manage().timeouts().implicitlyWait(this.getImplicitWait(), TimeUnit.SECONDS);
        driverThreadLocal.set(driver);
        return driver;
    }

    public String getBrowser() {
        return browser;
    }

    public String getBrowserVersion() {
        return browserVersion;
    }

    public String getChromeDriverPath() {
        return chromeDriverPath;
    }

    public WebDriver getDriver() {
        return driver;
    }

    public int getExplicitWait() {
        if(explicitWaitTimeout < getImplicitWait())
        {
            return getImplicitWait();
        }else
            return explicitWaitTimeout;
    }

    public String getFfBinPath() {
        return ffBinPath;
    }

    public String getFfProfilePath() throws URISyntaxException {
        if (ffProfilePath == null && getClass().getResource("/profiles/customProfileDirCUSTFF")!=null) {

            return getClass().getResource("/profiles/customProfileDirCUSTFF").toURI().getPath();
        } else
            return ffProfilePath;
    }

    public String getHubUrl() {
        return hubUrl;
    }

    public String getIEDriverPath() {
        return ieDriverPath;
    }

    public int getImplicitWait() {
        return implicitWaitTimeout;
    }

    public String getMode() {
        return mode;
    }

    public String getOutputDirectory() {
        return outputDirectory;
    }

     private String getUserAgentOverride() {
        return ContextManager.getContext().getPCSettings() + " Mozilla/5.0 (Windows; U; Windows NT 5.0; en-US; rv:1.9.1.7) Gecko/20091221 Firefox/3.5.7";
    }

    public int getWebSessionTimeout() {
        return WebSessionTimeout;
    }


    private void init() {
        browser = ContextManager.getContext().getWebRunBrowser();
        mode = ContextManager.getContext().getWebRunMode();
        hubUrl = ContextManager.getContext().getWebDriverGrid();
        ReportFactory.printAndLog(LogType.Info, "+++++++++++++++ Hub URL is+++++++++++"+hubUrl);
        ffProfilePath = ContextManager.getContext().getFirefoxUserProfilePath();
        ffBinPath = ContextManager.getContext().getFirefoxBinPath();
        chromeDriverPath = ContextManager.getContext().getChromeDriverPath();
        ieDriverPath = ContextManager.getContext().getIEDriverPath();
        WebSessionTimeout = ContextManager.getContext().getWebSessionTimeout();
        implicitWaitTimeout = ContextManager.getContext().getImplicitWaitTimeout();
        explicitWaitTimeout = ContextManager.getContext().getExplicitWaitTimeout();
        outputDirectory = ContextManager.getContext().getTestNGDirectory();
        browserVersion = ContextManager.getContext().getWebBrowserVersion();
        platform = ContextManager.getContext().getPlatform();
        this.setAssumeUntrustedCertificateIssuer  = ContextManager.getContext().ignoreUnTrustedCertificates();
        this.enableJavascript =  ContextManager.getContext().isJavaScriptEnabled();
    }

    public boolean isSetAcceptUntrustedCertificates() {
        return setAcceptUntrustedCertificates;
    }

    public boolean isSetAssumeUntrustedCertificateIssuer() {
        return setAssumeUntrustedCertificateIssuer;
    }

    private String readFileAsString(File file) throws java.io.IOException {
        StringBuffer fileData = new StringBuffer(1000);
        BufferedReader reader = new BufferedReader(new FileReader(file));
        char[] buf = new char[1024];
        int numRead = 0;
        while ((numRead = reader.read(buf)) != -1) {
            String readData = String.valueOf(buf, 0, numRead);
            fileData.append(readData);
            buf = new char[1024];
        }
        reader.close();
        return fileData.toString();
    }



    public void setBrowser(String browser) {
        this.browser = browser;
    }

    public void setBrowserVersion(String browserVersion) {
        this.browserVersion = browserVersion;
    }

    public void setChromeDriverPath(String chromeDriverPath) {
        this.chromeDriverPath = chromeDriverPath;
    }

    public void setFfBinPath(String ffBinPath) {
        this.ffBinPath = ffBinPath;
    }

    public void setFfProfilePath(String ffProfilePath) {
        this.ffProfilePath = ffProfilePath;
    }

    public void setHubUrl(String hubUrl) {
        this.hubUrl = hubUrl;
    }

    public void setIEDriverPath(String ieDriverPath) {
        this.ieDriverPath = ieDriverPath;
    }

    public void setImplicitlyWaitTimeout(int implicitTimeout) {
        this.implicitWaitTimeout = implicitTimeout;
    }

    public void setMode(String mode) {
        this.mode = mode;
    }

    public void setOutputDirectory(String outputDirectory) {
        this.outputDirectory = outputDirectory;
    }

    public void setSetAcceptUntrustedCertificates(boolean setAcceptUntrustedCertificates) {
        this.setAcceptUntrustedCertificates = setAcceptUntrustedCertificates;
    }

    public void setSetAssumeUntrustedCertificateIssuer(boolean setAssumeUntrustedCertificateIssuer) {
        this.setAssumeUntrustedCertificateIssuer = setAssumeUntrustedCertificateIssuer;
    }

    public void setWebSessionTimeout(int webSessionTimeout) {
        WebSessionTimeout = webSessionTimeout;
    }

    public static WebElement findElementWithTimeout(By locator) throws TimeoutException {
        WebDriver driver = WebUIDriver.getWebDriver(false);
        driver.manage().timeouts().implicitlyWait(0, TimeUnit.SECONDS); 

        return (new WebDriverWait(driver, 10)
                .ignoring(NoSuchElementException.class))
                .until(ExpectedConditions.presenceOfElementLocated(locator));
    }
}
