package com.complianceease.framework.database;

import java.sql.SQLException;

import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;

import com.complianceease.framework.api.enums.LogType;
import com.complianceease.framework.common.ReportFactory;
import com.complianceease.framework.common.models.LoginAccount;

public class QueriesRepository {

	// Get Credentials
	public static LoginAccount FindCredentials(int disclosureType, String environment) throws SQLException {
		ModelMapper modelMapper = new ModelMapper();

		String query = String.format("SELECT* FROM Credentials WHERE CredentialType=%d AND Environment='%s'",
				disclosureType, environment);

		DBRequest db = new DBRequest();
		ResultTable result = db.query(query);
		TableRow row = result.getTableRow(1);

		LoginAccount loginAccount = modelMapper.map(row.toMap(), new TypeToken<LoginAccount>() {}.getType());
		ReportFactory.printAndLog(LogType.Info, "<b>Invonking query: </b></br><pre>" + query + "</pre>");

		db.release();
		return loginAccount;

	}
	
	//Get Credentials
		public static LoginAccount getCredentialsByUserName(String userName,String environment) throws SQLException
		{
			ModelMapper modelMapper = new ModelMapper();

			String query= String.format("SELECT * FROM Credentials WHERE UserName='%s' AND Environment='%s'", userName, environment);

			DBRequest db = new DBRequest();
			ResultTable result = db.query(query);
			TableRow row = result.getTableRow(1);

			LoginAccount loginAccount = modelMapper.map(row.toMap(), new TypeToken<LoginAccount>(){}.getType());
			ReportFactory.printAndLog(LogType.Info,"<b>Invonking query: </b></br><pre>"+ query +"</pre>" );

			db.release();
			return loginAccount;

		}
}
