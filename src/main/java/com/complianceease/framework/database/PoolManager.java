package com.complianceease.framework.database;

import java.sql.Connection;
import java.sql.SQLException;

import com.complianceease.framework.common.configuration.ContextManager;
import com.zaxxer.hikari.HikariConfig;  
import com.zaxxer.hikari.HikariDataSource; 


public class PoolManager {
	
	public static PoolManager getInstance = new PoolManager();
	
	public static Boolean isInitialized(){ return  IsInitialized; }
	
	private static Boolean IsInitialized = false;
	
	private static HikariDataSource ds;

	private PoolManager() {}

    public static void init(){
    	ContextManager context = ContextManager.getContext();
    	String url = context.getSQLServerUrl();
    	String login = context.getSQLServerLogin();
    	String password = context.getSQLServerPassword();
    	String driver = context.getSQLServerDriver();
    	int minPoolSize = 1;
    	int maxPoolSize = 20;
        
        HikariConfig config = new HikariConfig();  
        config.setDriverClassName(driver);
        config.setJdbcUrl(url);
        config.setUsername(login);
        config.setPassword(password);
        config.addDataSourceProperty("cachePrepStmts", true);  
        config.addDataSourceProperty("prepStmtCacheSize", 500);  
        config.addDataSourceProperty("prepStmtCacheSqlLimit", 2048);  
        config.setConnectionTestQuery("SELECT 1");  
        config.setAutoCommit(true);  
        config.setMinimumIdle(minPoolSize);  
        config.setMaximumPoolSize(maxPoolSize);  
        config.setConnectionTimeout(10001);
        System.out.println("DS configuraiton"+config);
		System.out.println("Connection URL"+url);
        ds = new HikariDataSource(config);  
        IsInitialized = true;
    }  
      
    public void shutdown(){  
    	ds.close();
    }  
      
    public static Connection getConnection(){
        try {  
        	if (!IsInitialized)
        		init();
            return ds.getConnection();  
        } catch (SQLException e) {  
            e.printStackTrace();  
            ds.resumePool();  
            return null;  
        }  
    }  

}
