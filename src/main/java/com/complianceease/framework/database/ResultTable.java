package com.complianceease.framework.database;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

import com.complianceease.framework.database.TableRow;


/**
 * <P>A table of data representing a database result set, which is  generated
 * by executing a statement that queries the database. this is a extension of ResultSet.
 */
public class ResultTable {
	
	 private ResultSet result;
	 private TableRow resultRow;
	
	 public ResultTable(ResultSet _result) {
		 this.result = _result;
	 }
	  
	 
	 /**
	  * <P>Retrieves the row of the designated in the current <B>ResultTable</B>
	  * @param rowIndex - the first row is 1, the second row is 2 ,... 
	  * @return the row
	  */
	 public TableRow getTableRow(int rowIndex) 
	 {
		 this.resultRow = new TableRow(rowIndex);
		 TableRow.ResultSet = this.result; 
		 return resultRow;	
	 } 
	 
	 public List<TableRow>  list()
	 {	 
		 List<TableRow> tableRowList = new ArrayList<>();
		 
		 try {
			 
			int cursor = 1;
			 
			while(this.result.next())
			 {
				
				 this.resultRow = new TableRow(cursor);
				 TableRow.ResultSet = this.result; 
				 tableRowList.add(resultRow); 
				 cursor++;
			 }
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		 return tableRowList;
	 }

}


