package com.complianceease.framework.database;

public enum AccountType
{
	
	        RegressRegulator(1),
	    	RegressSeller(2),
	    	Regress(3),
	    	RegressBuyer(4),
	    	RegressConsumer(5),
	    	RegressConsumerUser(6),
	    	Test(7),
	    	RegressA2Admin(8),
	    	RegressEAdmin(9),
	    	RegressPayday(10),
	    	Autobatch1(11),
	    	RegressLicensee(12),
	    	RegressYogesh(14) {
				public String toString() {
					return "regress-yogesh";
				}
			},
			usercacheTest(16),
	    	user083001(15),
	        root(18);

			private int value;
	    	 
	    	 private AccountType(int value) {
	    	        this.value = value;
	    	    }
	    	 
	    	 public int getValue() {
	    	        return value;
	    	    }
}
