package com.complianceease.framework.database;

import java.sql.*;
import java.util.HashMap;
import java.util.Map;

/**
 * <P>Representation of a row of a ResultTable
 */
public class TableRow {

	 public static ResultSet ResultSet;	 
	 private int Cursor;
	
	 //Constructor
	 public TableRow(int _cursor) {
		 this.Cursor = _cursor;
	 }
	 	
	 /**
	  * <P>Retrieves the value of the designated column in the current row of this <B>TableRow</B>
	  * @param columnName the label for the column specified with the SQL AS clause.
	  * @return the column value; if the value is SQL NULL, the value returned is null
	  * @exception SQLException if a database access error occurs or this method is
     *            called on a closed result set
	  */
	 public String getColunmValue(String columnName) throws SQLException {	
		 
		 String value = "";
		 
		 try {
			 
			TableRow.ResultSet.absolute(this.Cursor);		
			value = TableRow.ResultSet.getString(columnName);	
			
		}catch (SQLException ex) {
			
			System.out.println("SQL Error Code : " + ex.getErrorCode() + 
			"; " + ex.getMessage() + ex.getStackTrace());
		}
		 
		return value;	 
	 }
	 
   public int getIntValue(String columnName) throws SQLException {	
		 
		 int ivalue = 0;
		 
		 try {
			 
			TableRow.ResultSet.absolute(this.Cursor);		
			String svalue = TableRow.ResultSet.getString(columnName);	
			if(!svalue.isEmpty())
				ivalue = Integer.parseInt(svalue);
			
		}catch (SQLException ex) {
			
			System.out.println("SQL Error Code : " + ex.getErrorCode() + 
			"; " + ex.getMessage() + ex.getStackTrace());
		}
		 
		return ivalue;	 
	 }
   
   public Boolean getBooleanValue(String columnName) throws SQLException {	
		 
		 Boolean value = false;
		 
		 try {
			 
			TableRow.ResultSet.absolute(this.Cursor);		
			value = TableRow.ResultSet.getBoolean(columnName);	
					
		}catch (SQLException ex) {
			
			System.out.println("SQL Error Code : " + ex.getErrorCode() + 
			"; " + ex.getMessage() + ex.getStackTrace());
		}
		 
		return value;	 
	 }
	 
	 public Map<String, String> toMap() throws SQLException {
		 
		 Map<String, String> parameters = new HashMap<String, String>();
	
		 try {
			
		    TableRow.ResultSet.absolute(this.Cursor);		
		    	
			ResultSetMetaData rsmd = TableRow.ResultSet.getMetaData();
			int columnCount = rsmd.getColumnCount();
				
			for (int i=1;i <= columnCount;i++)
			{
				String columnName = rsmd.getColumnName(i);
				String columnValue = TableRow.ResultSet.getString(columnName);
				String value =  columnValue != null ? columnValue.trim() : "";			
				parameters.put(columnName,value);
			}			
			
		}catch (SQLException ex) {
				
				System.out.println("SQL Error Code : " + ex.getErrorCode() + 
				"; " + ex.getMessage() + ex.getStackTrace());
		}
		 
		 return parameters;
	 }
	 
	 
}